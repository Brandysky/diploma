package cz.miro.diploma.diploma_be;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootTest
class DiplomaBeApplicationTests {

    @Test
    void contextLoads() {
    }

}
