package cz.miro.diploma.diploma_be.common.res;

import cz.miro.diploma.diploma_be.common.mvc.OcCommonService;
import cz.miro.diploma.diploma_be.common.validation.ValidationInfo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.transaction.SystemException;
import javax.validation.ValidationException;
import java.util.Date;

@ControllerAdvice
@AllArgsConstructor
public class CommonControllerAdvice extends ResponseEntityExceptionHandler {

    private final OcCommonService ocCommonService;


    @ExceptionHandler(value = {ValidationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ValidationInfo validationException(ValidationException ex) {


        return ocCommonService.convertValidationException(ex);
    }


}
