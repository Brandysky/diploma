package cz.miro.diploma.diploma_be.common.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.miro.diploma.diploma_be.common.mvc.OcCommonService;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import java.util.Locale;

public class ValidationMessage {

    /**
     * e.g. name/id of input used in app frontend (username, password ...)
     */
    private String propertyPath;

    /**
     * Message text
     */
    private String message;

    /**
     * Localized error code (typically from messages bundle)
     */
    private String errorCode;

    /**
     * Error code params
     */
    private Object[] params;

    public ValidationMessage() {
    }

    public ValidationMessage(String message) {
        this.message = message;
    }

    public ValidationMessage(String propertyPath, String message) {
        this.propertyPath = propertyPath;
        this.message = message;
    }

    public ValidationMessage(String errorCode, Object... params) {
        this.errorCode = errorCode;
        this.params = params;
    }

    public ValidationMessage(String propertyPath, String errorCode, Object... params) {
        this.propertyPath = propertyPath;
        this.errorCode = errorCode;
        this.params = params;
    }

    public ValidationMessage(ConstraintViolation<?> cv) {
        this(cv.getMessage());

        propertyPath = cv.getPropertyPath().toString();
    }

    public ValidationMessage resolveMessage(OcCommonService ocCommonService, Locale locale) {
        if (StringUtils.isNotBlank(getErrorCode())) {
            setMessage(ocCommonService.format(getErrorCode(), locale, getParams()));
            setErrorCode(null);
        }
        return this;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    public void setPropertyPath(String propertyPath) {
        this.propertyPath = propertyPath;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @JsonIgnore
    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
