package cz.miro.diploma.diploma_be.domain.device;

import cz.miro.diploma.diploma_be.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {

    Device findByDeviceToken(String deviceToken);




}
