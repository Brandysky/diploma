package cz.miro.diploma.diploma_be.common.util;

import java.text.Normalizer;
import java.util.Date;

public class Utils {


    public static String stripDiacritics(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        return str;
    }

    public static boolean isBeforeToday(Date date){
        return date.before(new Date());
    }
}
