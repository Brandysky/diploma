package cz.miro.diploma.diploma_be.configuration.security.jwt;

import cz.miro.diploma.diploma_be.common.util.Constant;
import cz.miro.diploma.diploma_be.configuration.security.services.UserDetailsServiceImpl;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class AuthTokenFilter extends OncePerRequestFilter {
	private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);


	@Autowired
	private JwtUtils jwtUtils;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;



	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> permited = new ArrayList<>();
		permited.add("/ping");
		permited.add("/api/login/**");

//		System.out.println(request.getServletPath()+ " containes:");
		AntPathMatcher matcher = new AntPathMatcher();
		for (String url : permited) {
			if(matcher.match(url, request.getServletPath())){
//				System.out.println("ANO") ;
				return true;
			}
		}
//		System.out.println("NE");
		return false;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = parseJwt(request);
			if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
				Integer id = jwtUtils.getUserIdFromJwtToken(jwt);

				UserDetails userDetails = userDetailsService.loadUserById(id);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (ExpiredJwtException e){
			throw e;
		}
		catch (Exception e) {
			logger.error("Cannot set user authentication: {}", e.getMessage());
		}

		filterChain.doFilter(request, response);
	}

	public static String parseJwt(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		if (StringUtils.hasText(token) && token.startsWith(Constant.TOKEN_PREFIX)) {
			return token.replace(Constant.TOKEN_PREFIX, "");
		}

		return null;
	}
}
