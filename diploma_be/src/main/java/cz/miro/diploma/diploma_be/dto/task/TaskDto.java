package cz.miro.diploma.diploma_be.dto.task;


import com.fasterxml.jackson.annotation.JsonFormat;
import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.enums.REPEAT_INTERVAL;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TaskDto {

    private Integer id;
    private Integer order;
    private String name;
    private String body;
    private Boolean done;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dateCreated;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dueDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date doneDate;

    private String latitude;
    private String longitude;
    private Boolean useGPS;
    private Boolean remind;
    private REPEAT_INTERVAL repeatInterval;
    private User user;
    private List<TaskItemDto> items = new ArrayList<>();
    private TaskStyleDto taskStyle; //todo

}
