package cz.miro.diploma.diploma_be.controller;

import cz.miro.diploma.diploma_be.configuration.security.jwt.JwtUtils;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import cz.miro.diploma.diploma_be.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@AllArgsConstructor
public class TaskController {


    JwtUtils jwtUtils;
    TaskService taskService;

    public void createTask() {

    }


    @PostMapping("/new")
    public void createTask(@RequestBody TaskDto task, @RequestParam(name = "boardId") Integer boardId, HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        taskService.createNewTask(boardId, task, user);
    }

    @PostMapping("/update")
    public void updateTask(@RequestBody TaskDto task, HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        taskService.updateTask(task, user);
    }

    @PostMapping("/updateOrder")
    public void reorderTasks(@RequestParam(name = "oldItemIndex") Integer oldItemIndex, @RequestParam(name = "oldListIndex") Integer oldListIndex, @RequestParam(name = "newItemIndex") Integer newItemIndex, @RequestParam(name = "newListIndex") Integer newListIndex, HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);
        taskService.reorderTasks(oldItemIndex, oldListIndex, newItemIndex, newListIndex, user);
    }
    @PostMapping("/delete")
    public void deleteTask(@RequestParam(name = "taskId") Integer taskId) {

        taskService.deleteTask(taskId);
    }

}
