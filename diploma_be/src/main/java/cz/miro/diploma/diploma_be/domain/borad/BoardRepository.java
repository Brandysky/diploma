package cz.miro.diploma.diploma_be.domain.borad;

import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BoardRepository extends JpaRepository<Board, Integer> {


    List<Board> findAllByUser(User u);
    Board findByOrder(Integer order);




}
