package cz.miro.diploma.diploma_be.domain.userActivity;

import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.enums.USER_ACTIVITY_TYPE;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;


@Entity(name = UserActivity.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class UserActivity {

    public static final String TABLE_NAME = "user_activity";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_USER= "user";
    public static final String PARAM_TYPE= "type";
    public static final String PARAM_DATE= "date";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_NAME)
    private String name;

    @Column(name = PARAM_DATE)
    private Date date;

    @Enumerated(EnumType.STRING)
    @Column(name = PARAM_TYPE)
    private USER_ACTIVITY_TYPE type;

    @ManyToOne
    @JoinColumn(name = PARAM_USER,nullable = false)
    private User user;




}
