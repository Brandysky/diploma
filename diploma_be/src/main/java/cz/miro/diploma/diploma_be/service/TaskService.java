package cz.miro.diploma.diploma_be.service;


import cz.miro.diploma.diploma_be.common.exception.OcValidationException;
import cz.miro.diploma.diploma_be.common.util.Constant;
import cz.miro.diploma.diploma_be.common.validation.ValidationMessage;
import cz.miro.diploma.diploma_be.domain.borad.Board;
import cz.miro.diploma.diploma_be.domain.borad.BoardRepository;
import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.task.TaskRepository;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItemRepository;
import cz.miro.diploma.diploma_be.domain.taskStyle.TaskStyle;
import cz.miro.diploma.diploma_be.domain.taskStyle.TaskStyleRepository;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.domain.user.UserRepository;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import cz.miro.diploma.diploma_be.dto.task.TaskItemDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TaskService {

    TaskRepository taskRepository;
    TaskItemRepository taskItemRepository;
    TaskStyleRepository taskStyleRepository;
    BoardRepository boardRepository;


    @Transactional
    public void createNewTask(Integer boardId, TaskDto taskDto, User user) {

        Board board = boardRepository.findById(boardId).orElse(null);
        if (board == null) {
            throw new OcValidationException("Failed to find Board with id " + boardId);
        }

        if (board.getUser() != user) {
            throw new OcValidationException("This user is not owner of this board");
        }


        Integer userBoardsCount = board.getTasks().size();

        Task newTask = new Task();
        newTask.setName(taskDto.getName())
                .setBody(taskDto.getBody())
                .setDone(taskDto.getDone())
                .setDateCreated(new Date())
                .setDueDate(taskDto.getDueDate())
                .setDoneDate(null)
                .setLatitude(taskDto.getLatitude())
                .setLongitude(taskDto.getLongitude())
                .setUseGPS(taskDto.getUseGPS())
                .setRemind(taskDto.getRemind())
                .setRepeatInterval(taskDto.getRepeatInterval())
                .setBoard(board)
                .setOrder(userBoardsCount);
        taskRepository.save(newTask);

        board.getTasks().add(newTask);
        boardRepository.save(board);

        if (taskDto.getItems() != null) {
            for (TaskItemDto item : taskDto.getItems()) {
                TaskItem newItem = new TaskItem();
                newItem.setBody(item.getBody())
                        .setTask(newTask)
                        .setDone(item.getDone())
                        .setDateCreated(new Date());
                taskItemRepository.save(newItem);
                newTask.getItems().add(newItem);
            }
        }

        TaskStyle taskStyle = new TaskStyle();
        taskStyle.setTask(newTask);
        taskStyleRepository.save(taskStyle);
        newTask.setTaskStyle(taskStyle);

        taskRepository.save(newTask);
    }


    @Transactional
    public void updateTask(TaskDto taskDto, User user) {

        Task task = taskRepository.findById(taskDto.getId()).orElse(null);

        if (task == null) {
            throw new RuntimeException("Task with id " + taskDto.getId() + " not found");
        }

        List<TaskItem> storedItems = new ArrayList<>(task.getItems());

        task
                .setName(taskDto.getName())
                .setBody(taskDto.getBody())
                .setDone(taskDto.getDone())
                .setDateCreated(taskDto.getDateCreated())
                .setDueDate(taskDto.getDueDate())
                .setDoneDate(taskDto.getDoneDate())
                .setLatitude(taskDto.getLatitude())
                .setLongitude(taskDto.getLongitude())
                .setUseGPS(taskDto.getUseGPS())
                .setRemind(taskDto.getRemind())
                .setRepeatInterval(taskDto.getRepeatInterval());

        for (TaskItemDto item : taskDto.getItems()) {
            TaskItem taskItem = taskItemRepository.findById(taskDto.getId()).orElse(null);
            if (taskItem == null) {
                //neexistuje v DB -> vytvořit
                taskItem = new TaskItem();
                taskItem.setBody(item.getBody())
                        .setTask(task)
                        .setDone(item.getDone())
                        .setDateCreated(new Date());

            } else {

                storedItems.remove(taskItem);
                //existuje v DB -> update
                taskItem.setBody(item.getBody())
                        .setTask(task)
                        .setDone(item.getDone())
                        .setDoneDate(item.getDoneDate());
            }
            taskItemRepository.save(taskItem);
            task.getItems().add(taskItem);
        }
        storedItems.forEach(task.getItems()::remove);
        taskItemRepository.deleteAll(storedItems);

        TaskStyle taskStyle = task.getTaskStyle();
        //todo set


        taskRepository.save(task);

    }

    @Transactional
    public void deleteTask(Integer taskId){
        Task task = taskRepository.findById(taskId).orElse(null);

        if (task == null) {
            throw new RuntimeException("Task with id " + taskId + " not found");
        }

        taskItemRepository.deleteAll(task.getItems());
        taskStyleRepository.delete(task.getTaskStyle());
        task.getBoard().getTasks().remove(task);
        boardRepository.save(task.getBoard());

        taskRepository.delete(task);

    }

    @Transactional
    public void reorderTasks(Integer oldItemIndex, Integer oldListIndex, Integer newItemIndex, Integer newListIndex, User user) {
        List<Board> boards = boardRepository.findAllByUser(user);

        Board oldBoard = boards.stream().filter(b -> b.getOrder().equals(oldListIndex)).findFirst().orElse(null);
        if (oldBoard == null) {
            throw new OcValidationException("failed to find board with order " + oldListIndex);
        }
        Board newBoard = boards.stream().filter(b -> b.getOrder().equals(newListIndex)).findFirst().orElse(null);
        if (newBoard == null) {
            throw new OcValidationException("failed to find board with order " + newItemIndex);
        }

        Task movedTask = oldBoard.getTasks().stream().filter(t -> t.getOrder().equals(oldItemIndex)).findFirst().orElse(null);
        if (movedTask == null) {
            throw new OcValidationException("failed to find task with order " + oldItemIndex + " in board with order " + oldListIndex);
        }
        // přesouvání
        if(oldListIndex.equals(newListIndex)){
            //změna ve stejném Boardu
            if (oldItemIndex > newItemIndex) {
                //posouvám dopředu
                for (Task task : oldBoard.getTasks()) {
                    if (task.getOrder() <= oldItemIndex && task.getOrder() >= newItemIndex) {
                        task.setOrder(task.getOrder()+1);
                    }
                }
            } else {
                //posouvám dozadu
                for (Task task : oldBoard.getTasks()) {
                    if (task.getOrder() >= oldItemIndex && task.getOrder() <= newItemIndex) {
                        task.setOrder(task.getOrder()-1);
                    }
                }
            }
            movedTask.setOrder(newItemIndex);
        }else{


            //srovnání ve starém boardu
            for (Task task : oldBoard.getTasks()) {
                if (task.getOrder() >= oldItemIndex) {
                    task.setOrder(task.getOrder()-1);
                }
            }
            for (Task task : newBoard.getTasks()) {
                if (task.getOrder() >= newItemIndex) {
                    task.setOrder(task.getOrder()+1);
                }
            }
            movedTask.setBoard(newBoard);
            movedTask.setOrder(newItemIndex);
        }

        boardRepository.saveAll(boards);
    }

}

