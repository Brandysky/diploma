package cz.miro.diploma.diploma_be.domain.taskStyle;

import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TaskStyleRepository extends JpaRepository<TaskStyle, Integer> {






}
