package cz.miro.diploma.diploma_be.configuration.security.services;

import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.domain.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final PasswordEncoder encoder;
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(PasswordEncoder encoder, UserRepository userRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(login);
        return UserDetailsImpl.build(user);
    }

    @Transactional
    public UserDetails loadUserById(Integer id) throws UsernameNotFoundException {
        User user = userRepository.findUserById(id);
        return UserDetailsImpl.build(user);
    }

}
