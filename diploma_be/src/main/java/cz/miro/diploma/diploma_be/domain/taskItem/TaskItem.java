package cz.miro.diploma.diploma_be.domain.taskItem;


import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.dto.task.TaskItemDto;
import cz.miro.diploma.diploma_be.enums.REPEAT_INTERVAL;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Entity(name = TaskItem.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class TaskItem {
    public static final String TABLE_NAME = "task_item";
    public static final String PARAM_ID = "id";
    public static final String PARAM_TASK = "task";
    public static final String PARAM_BODY = "body";
    public static final String PARAM_DONE = "done";
    public static final String PARAM_DATE_CREATED = "date_created";
    public static final String PARAM_DONE_DATE = "done_date";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_BODY)
    private String body;

    @Column(name = PARAM_DONE)
    private Boolean done;

    @Column(name = PARAM_DATE_CREATED)
    private Date dateCreated;

    @Column(name = PARAM_DONE_DATE)
    private Date doneDate;

    @ManyToOne
    @JoinColumn(name = PARAM_TASK, nullable = false)
    private Task task;

    public TaskItemDto convertToDto() {
        TaskItemDto dto = new TaskItemDto();
        dto.setId(id)
                .setBody(body)
                .setDone(done)
                .setDateCreated(dateCreated)
                .setDoneDate(doneDate);

        return dto;

    }
}
