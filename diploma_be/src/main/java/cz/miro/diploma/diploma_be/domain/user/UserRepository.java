package cz.miro.diploma.diploma_be.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserById(Integer Id);

//    User findUserByMobileNumber(String mobileNumber);
    User findUserByLogin(String login);




}
