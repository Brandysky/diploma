package cz.miro.diploma.diploma_be.domain.taskStyle;


import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.dto.task.TaskStyleDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Entity(name = TaskStyle.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class TaskStyle {
    public static final String TABLE_NAME = "task_style";
    public static final String PARAM_ID = "id";
    public static final String PARAM_TASK = "task";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = PARAM_TASK,
            referencedColumnName = Task.PARAM_ID)
    private Task task;

    public TaskStyleDto convertToDto(){
        TaskStyleDto dto= new TaskStyleDto();

        return dto;

    }
}
