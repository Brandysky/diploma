package cz.miro.diploma.diploma_be.dto.task;


import com.fasterxml.jackson.annotation.JsonFormat;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.util.Date;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TaskItemDto {

    private Integer id;
    private String body;
    private Boolean done;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dateCreated;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date doneDate;


    public TaskItem convertToEntity() {

        TaskItem taskItem = new TaskItem();
        taskItem.setId(id)
                .setBody(body)
                .setDone(done)
                .setDateCreated(dateCreated)
                .setDoneDate(doneDate);

        return taskItem;
    }
}
