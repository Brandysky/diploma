package cz.miro.diploma.diploma_be.dto.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class LoginRequest {
    @NotBlank
    private String login;

    @NotBlank
    private String password;


}
