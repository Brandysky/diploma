package cz.miro.diploma.diploma_be.domain.category;


import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.enums.REPEAT_INTERVAL;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = Category.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Category {
    public static final String TABLE_NAME = "category";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_USER = "user";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_NAME)
    private String name;

    @ManyToOne
    @JoinColumn(name = PARAM_USER,nullable = false)
    private User user;

}
