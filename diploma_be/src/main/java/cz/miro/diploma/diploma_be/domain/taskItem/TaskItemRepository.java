package cz.miro.diploma.diploma_be.domain.taskItem;

import cz.miro.diploma.diploma_be.domain.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TaskItemRepository extends JpaRepository<TaskItem, Integer> {






}
