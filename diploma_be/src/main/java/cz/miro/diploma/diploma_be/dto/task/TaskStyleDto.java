package cz.miro.diploma.diploma_be.dto.task;


import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.taskStyle.TaskStyle;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TaskStyleDto {

    private Integer id;

    public TaskStyle convertToEntity(){
        TaskStyle taskStyle = new TaskStyle();
        taskStyle.setId(id);

        return taskStyle;
    }

}
