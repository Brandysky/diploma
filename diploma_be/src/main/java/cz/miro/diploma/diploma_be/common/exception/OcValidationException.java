package cz.miro.diploma.diploma_be.common.exception;

import cz.miro.diploma.diploma_be.common.validation.ValidationMessage;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class OcValidationException extends ValidationException {

    private Collection<ValidationMessage> messages = new ArrayList<>();

    public OcValidationException(ValidationMessage... msgs) {
        if (msgs != null && msgs.length > 0) {
            messages = Arrays.asList(msgs);
        }
    }

    public OcValidationException(Collection<ValidationMessage> messages) {
        this.messages = messages;
    }

    public OcValidationException(String message) {
        messages.add(new ValidationMessage(message));
    }

    public OcValidationException(String message, String propertyPath) {
        messages.add(new ValidationMessage(propertyPath, message));
    }

    public Collection<ValidationMessage> getMessages() {
        return messages;
    }

    @Override
    public String getMessage() {
        return String.join(", ", messages.stream().map(ValidationMessage::getMessage).collect(Collectors.toSet()));
    }
}
