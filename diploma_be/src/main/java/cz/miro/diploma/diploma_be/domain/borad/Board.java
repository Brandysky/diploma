package cz.miro.diploma.diploma_be.domain.borad;


import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.dto.task.BoardDto;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = cz.miro.diploma.diploma_be.domain.borad.Board.TABLE_NAME)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Board {

    public static final String TABLE_NAME = "board";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_USER = "user";
    public static final String PARAM_ORDER = "order_index";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_NAME)
    private String name;

    @Column(name = PARAM_ORDER)
    private Integer order;

    @ManyToOne
    @JoinColumn(name = PARAM_USER, nullable = false)
    private User user;

    @OneToMany(mappedBy = "board", cascade = {CascadeType.PERSIST, CascadeType.REFRESH},fetch = FetchType.EAGER)
    private Set<Task> tasks = new HashSet<>();

    public BoardDto convertToDto() {
        BoardDto dto = new BoardDto();
        dto.setId(id);
        dto.setName(name);
        dto.setOrder(order);
        dto.setTasks(tasks.stream().map(Task::convertToDto).sorted(Comparator.comparing(TaskDto::getOrder)).collect(Collectors.toList()));

        return dto;
    }

}



