package cz.miro.diploma.diploma_be.dto.task;

import cz.miro.diploma.diploma_be.domain.task.Task;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BoardDto {

    private Integer id;
    private String name;
    private Integer order;
    private List<TaskDto> tasks = new ArrayList<>();

}
