package cz.miro.diploma.diploma_be.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class SignupResponseDto {

    private String jwtToken;
    private String deviceToken;



    public SignupResponseDto(String jwtToken, String deviceToken) {
        this.jwtToken = jwtToken;
        this.deviceToken = deviceToken;
    }
}
