package cz.miro.diploma.diploma_be.common.util;

public final class Constant {

    public static final Object[] EMPTY_ARRAY = {};

    // TOKEN DEFAULTS
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
}
