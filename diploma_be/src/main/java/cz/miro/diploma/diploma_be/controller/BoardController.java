package cz.miro.diploma.diploma_be.controller;

import cz.miro.diploma.diploma_be.configuration.security.jwt.JwtUtils;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.dto.task.BoardDto;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import cz.miro.diploma.diploma_be.service.BoardService;
import cz.miro.diploma.diploma_be.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/board")
@AllArgsConstructor
public class BoardController {


    JwtUtils jwtUtils;
    BoardService boardService;


    @PostMapping("/new")
    public void createBoard(@RequestBody BoardDto boardDto, HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        boardService.createNewBoard(boardDto, user);
    }

    @PostMapping("/delete")
    public void deleteBoard(@RequestParam(name = "boardId") Integer boardId, HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        boardService.deleteBoard(boardId, user);
    }

    @PostMapping("/updateOrder")
    public void updateBoardOrder(@RequestParam(name = "oldIndex") Integer oldIndex, @RequestParam(name = "newIndex") Integer newIndex,HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);
        boardService.updateBoardOrder(oldIndex, newIndex, user);
    }

    @GetMapping("/all")
    public List<BoardDto> getAllBoards(HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        return boardService.getAllBoards(user);
    }

}
