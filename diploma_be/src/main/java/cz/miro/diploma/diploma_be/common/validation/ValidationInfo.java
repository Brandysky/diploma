package cz.miro.diploma.diploma_be.common.validation;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class ValidationInfo {

    private Collection<ValidationMessage> messages = new ArrayList<>();

    public ValidationInfo() {
    }

    public ValidationInfo(String messageStr) {
        messages.add(new ValidationMessage(messageStr));
    }

    public ValidationInfo(Collection<ValidationMessage> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return StringUtils.join(messages, ", ");
    }

    public Collection<ValidationMessage> getMessages() {
        return messages;
    }

    public void setMessages(Collection<ValidationMessage> messages) {
        this.messages = messages;
    }
}
