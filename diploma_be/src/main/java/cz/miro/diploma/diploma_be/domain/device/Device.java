package cz.miro.diploma.diploma_be.domain.device;

import cz.miro.diploma.diploma_be.domain.user.User;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.*;

@Entity(name = Device.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Device {
    public static final String TABLE_NAME = "device";
    public static final String PARAM_ID = "id";
    public static final String PARAM_USER = "user_id";
    public static final String PARAM_DEVICE_TOKEN = "device_token";
    public static final String PARAM_DATE_CREATED = "date_created";
    public static final String PARAM_DATE_REFRESHED = "date_refreshed";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_DEVICE_TOKEN)
    private String deviceToken;

    @Column(name = PARAM_DATE_CREATED)
    private Date dateCreated;

    @Column(name = PARAM_DATE_REFRESHED)
    private Date dateRefreshed;

    @ManyToOne
    @JoinColumn(name = PARAM_USER,nullable = false)
    private User user;
}
