package cz.miro.diploma.diploma_be.controller;

import cz.miro.diploma.diploma_be.configuration.security.jwt.JwtUtils;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.dto.auth.LoginRequest;
import cz.miro.diploma.diploma_be.dto.auth.SignupRequest;
import cz.miro.diploma.diploma_be.dto.auth.SignupResponseDto;
import cz.miro.diploma.diploma_be.dto.auth.TokenBodyDto;
import cz.miro.diploma.diploma_be.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/login")
@AllArgsConstructor
public class LoginController {

    private final LoginService loginService;
    JwtUtils jwtUtils;

    @PostMapping(value = "/signup")
    public SignupResponseDto registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return loginService.registerUser(signUpRequest);
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<?> logoutUser(HttpServletRequest request) {
        User user = jwtUtils.getUserFromRequest(request);

        return loginService.logoutUser(user);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        return loginService.loginUser(loginRequest);
    }

    @PostMapping("/refreshDeviceToken/{deviceToken}")
    public ResponseEntity<?> refreshDeviceToken(@PathVariable String deviceToken) {
        return loginService.refreshDeviceToken(deviceToken);
    }

    @PostMapping(value = "/checkToken")
    public ResponseEntity<?> checkToken(@RequestBody TokenBodyDto token) {
        return loginService.checkToken(token);
    }

}
