package cz.miro.diploma.diploma_be.service;

import cz.miro.diploma.diploma_be.common.exception.OcValidationException;
import cz.miro.diploma.diploma_be.common.util.Constant;
import cz.miro.diploma.diploma_be.common.validation.ValidationMessage;
import cz.miro.diploma.diploma_be.configuration.security.jwt.JwtUtils;
import cz.miro.diploma.diploma_be.domain.device.Device;
import cz.miro.diploma.diploma_be.domain.device.DeviceRepository;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.domain.user.UserRepository;
import cz.miro.diploma.diploma_be.dto.auth.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class LoginService {


    private final JwtUtils jwtUtils;
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private PasswordEncoder passwordEncoder;


    private String generateDeviceToken() {
        return UUID.randomUUID().toString();
    }

    public ResponseEntity<?> checkToken(TokenBodyDto tokenBody) {
        String prefix = "Bearer ";
        if (tokenBody.getToken() == null || !tokenBody.getToken().startsWith(prefix) || tokenBody.getToken().length() < 10)
            throw new OcValidationException(new ValidationMessage("user.login.jwtToken.notValid", Constant.EMPTY_ARRAY));
        String token = tokenBody.getToken().substring(prefix.length());
        System.out.println(token);
        boolean r = jwtUtils.validateJwtToken(token);
        System.out.println("token is valid: " + r);
        if (!r)
            throw new OcValidationException(new ValidationMessage("user.login.jwtToken.notValid", Constant.EMPTY_ARRAY));
        return ResponseEntity.ok().build();

    }

    public ResponseEntity<?> refreshDeviceToken(String deviceToken) {
        System.out.println("Device " + deviceToken);
        Device device = deviceRepository.findByDeviceToken(deviceToken);

        if (device == null)
            throw new OcValidationException(new ValidationMessage("user.login.token.notFound", Constant.EMPTY_ARRAY));

        if (device.getUser() == null)
            throw new OcValidationException(new ValidationMessage("user.login.token.user.notFound", Constant.EMPTY_ARRAY));

        String newDeviceToken = generateDeviceToken();

        device.setDeviceToken(newDeviceToken);
        device.setDateRefreshed(new Date());

        deviceRepository.save(device);
        String jwt = "Bearer " +jwtUtils.generateJwtToken(device.getUser());

        return ResponseEntity.ok(new SignupResponseDto(jwt,
                newDeviceToken));

    }

    public ResponseEntity<?> loginUser(LoginRequest loginRequest) {

        User user = userRepository.findUserByLogin(loginRequest.getLogin());


        if (user == null) {
            throw new OcValidationException(
                    new ValidationMessage("login", "user.login.notExist", Constant.EMPTY_ARRAY));
        }

        String passwordStored = user.getPassword();
        boolean passed = passwordEncoder.matches(loginRequest.getPassword(), passwordStored);
        if (!passed) {
            throw new OcValidationException(
                    new ValidationMessage("password", "user.login.password.invalid", Constant.EMPTY_ARRAY));
        }
        //vytvoření device tokenu -> uložení a přidělení ke klientovi

        String deviceToken = addDeviceToUser(user);
        String jwt = jwtUtils.generateJwtToken(user);


        return ResponseEntity.ok(new SignupResponseDto("Bearer " + jwt, deviceToken));
    }

    public ResponseEntity<?> logoutUser(User user) {

        return null;
    }



    @Transactional
    public String addDeviceToUser(User u){
        String deviceToken = generateDeviceToken();
        Device device = new Device();
        device.setDeviceToken(deviceToken)
                .setDateCreated(new Date())
                .setUser(u);

        deviceRepository.save(device);

        return deviceToken;
    }

    @Transactional
    public SignupResponseDto registerUser(SignupRequest signUpRequest) {

        User user = userRepository.findUserByLogin(signUpRequest.getLogin());
        boolean userExists = user != null;

        if (userExists) {
            throw new OcValidationException(
                    new ValidationMessage("login", "user.login.exist", Constant.EMPTY_ARRAY));
        }
        String password = signUpRequest.getPassword();
        String passwordEncoded = passwordEncoder.encode(password);

        //validace hesla - vyhodí exception
//        validatePassword(passwordEncoded);

        user = new User();
        user.setLogin(signUpRequest.getLogin())
                .setName(signUpRequest.getName())
                .setDateRegistered(new Date())
                .setPassword(passwordEncoded);

        userRepository.save(user);

        //vytvoření device tokenu -> uložení a přidělení ke klientovi

        String deviceToken = addDeviceToUser(user);

        //todo vytvoření kategorií
        String jwt = jwtUtils.generateJwtToken(user);

        SignupResponseDto response = new SignupResponseDto();
        response.setDeviceToken(deviceToken);
        response.setJwtToken("Bearer " + jwt);

        return response;
    }


}
