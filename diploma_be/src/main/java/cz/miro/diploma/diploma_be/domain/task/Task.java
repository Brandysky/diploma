package cz.miro.diploma.diploma_be.domain.task;


import cz.miro.diploma.diploma_be.domain.borad.Board;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import cz.miro.diploma.diploma_be.domain.taskStyle.TaskStyle;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import cz.miro.diploma.diploma_be.enums.REPEAT_INTERVAL;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = Task.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Task {
    public static final String TABLE_NAME = "task";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_BODY = "body";
    public static final String PARAM_DONE = "done";
    public static final String PARAM_DATE_CREATED = "date_created";
    public static final String PARAM_BOARD = "board";
    public static final String PARAM_DUE_DATE = "due_date";
    public static final String PARAM_DONE_DATE = "done_date";
    public static final String PARAM_LATITUDE = "latitude";
    public static final String PARAM_LONGITUDE = "longitude";
    public static final String PARAM_USE_GPS = "use_gps";
    public static final String PARAM_REMIND = "remind";
    public static final String PARAM_REPEAT_INTERVAL = "repeat_interval";
    public static final String PARAM_ORDER = "order_index";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_NAME)
    private String name;

    @Column(name = PARAM_BODY, length = 500)
    private String body;

    @Column(name = PARAM_DONE)
    private Boolean done;

    @Column(name = PARAM_DATE_CREATED)
    private Date dateCreated;

    @Column(name = PARAM_DUE_DATE)
    private Date dueDate;

    @Column(name = PARAM_DONE_DATE)
    private Date doneDate;

    @Column(name = PARAM_LATITUDE)
    private String latitude;

    @Column(name = PARAM_LONGITUDE)
    private String longitude;

    @Column(name = PARAM_USE_GPS)
    private Boolean useGPS;

    @Column(name = PARAM_REMIND)
    private Boolean remind;

    @Column(name = PARAM_ORDER)
    private Integer order;

    @Enumerated(EnumType.STRING)
    @Column(name = PARAM_REPEAT_INTERVAL)
    private REPEAT_INTERVAL repeatInterval;

    @ManyToOne
    @JoinColumn(name = PARAM_BOARD)
    private Board board;

    @OneToMany(mappedBy = "task", cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<TaskItem> items = new HashSet<>();

    @OneToOne(mappedBy = "task", cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private TaskStyle taskStyle;


    public TaskDto convertToDto() {
        TaskDto dto = new TaskDto();
        dto.setId(id)
                .setName(name)
                .setBody(body)
                .setDone(done)
                .setDateCreated(dateCreated)
                .setDueDate(dueDate)
                .setDoneDate(doneDate)
                .setLatitude(latitude)
                .setLongitude(longitude)
                .setOrder(order)
                .setUseGPS(useGPS)
                .setRemind(remind)
                .setRepeatInterval(repeatInterval)
//                .setUser(user)
                .setItems(items.stream().map(TaskItem::convertToDto).collect(Collectors.toList()));
//               .setTaskStyle(taskStyle.convertToDto());

        return dto;
    }
}
