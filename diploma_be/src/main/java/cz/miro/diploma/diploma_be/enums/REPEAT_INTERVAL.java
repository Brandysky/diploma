package cz.miro.diploma.diploma_be.enums;

public enum REPEAT_INTERVAL {
    DAILY, WEEKLY, MONTHLY,
}
