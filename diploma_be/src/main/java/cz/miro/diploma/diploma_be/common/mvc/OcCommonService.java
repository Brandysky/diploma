package cz.miro.diploma.diploma_be.common.mvc;

import cz.miro.diploma.diploma_be.common.exception.OcValidationException;
import cz.miro.diploma.diploma_be.common.exception.OcValidationException;
import cz.miro.diploma.diploma_be.common.validation.ValidationInfo;
import cz.miro.diploma.diploma_be.common.validation.ValidationMessage;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.Locale;

@Service
@AllArgsConstructor
public class OcCommonService {

    private final ApplicationContext applicationContext;


    public ValidationInfo convertValidationException(ValidationException ex) {

        ValidationInfo validationInfo = new ValidationInfo();

        if (ex instanceof ConstraintViolationException) {
            ConstraintViolationException cve = (ConstraintViolationException) ex;
            for (ConstraintViolation<?> cv : cve.getConstraintViolations()) {
                validationInfo.getMessages().add(new ValidationMessage(cv));
            }

        } else if (ex instanceof OcValidationException) {
            validationInfo.setMessages(((OcValidationException) ex).getMessages());

            for (ValidationMessage vm : validationInfo.getMessages()) {
                vm.resolveMessage(this, LocaleContextHolder.getLocale());
            }
        } else {
            validationInfo.getMessages().add(new ValidationMessage(ex.getMessage()));
        }

        return validationInfo;
    }

    public String format(String code, Locale locale, Object... args) {
        try {
            return applicationContext.getMessage(code, args, locale);
        } catch (NoSuchMessageException e) {
            return code;
        }
    }

    public String format(String code, Object... args) {
        try {
            return applicationContext.getMessage(code, args, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            return code;
        }
    }
}
