package cz.miro.diploma.diploma_be.domain.user;

import cz.miro.diploma.diploma_be.domain.borad.Board;
import cz.miro.diploma.diploma_be.domain.notification.Notification;
import cz.miro.diploma.diploma_be.domain.device.Device;
import cz.miro.diploma.diploma_be.domain.userActivity.UserActivity;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.*;


@Entity(name = User.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class User {

    public static final String TABLE_NAME = "user";
    public static final String PARAM_ID = "id";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_DATE_REGISTERED = "date_registered";
    public static final String PARAM_TOKEN = "token";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_NAME)
    private String name;

    @Column(name = PARAM_LOGIN)
    private String login;

    @Column(name = PARAM_PASSWORD)
    private String password;

    @Column(name = PARAM_DATE_REGISTERED)
    private Date dateRegistered;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Board> boards = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Device> tokens = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<UserActivity> activities = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Notification> notifications = new HashSet<>();
}
