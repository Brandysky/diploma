package cz.miro.diploma.diploma_be.dto.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class TokenBodyDto {

    @NotNull
    private String token;

}
