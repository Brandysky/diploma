package cz.miro.diploma.diploma_be.service;


import cz.miro.diploma.diploma_be.common.exception.OcValidationException;
import cz.miro.diploma.diploma_be.domain.borad.Board;
import cz.miro.diploma.diploma_be.domain.borad.BoardRepository;
import cz.miro.diploma.diploma_be.domain.task.Task;
import cz.miro.diploma.diploma_be.domain.task.TaskRepository;
import cz.miro.diploma.diploma_be.domain.taskItem.TaskItem;
import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.domain.user.UserRepository;
import cz.miro.diploma.diploma_be.dto.task.BoardDto;
import cz.miro.diploma.diploma_be.dto.task.TaskDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BoardService {


    BoardRepository boardRepository;
    TaskRepository taskRepository;
    UserRepository userRepository;

    @Transactional
    public void createNewBoard(BoardDto boardDto, User user) {
        Integer userBoardsCount = boardRepository.findAllByUser(user).size();
        Board newBoard = new Board();
        newBoard.setName(boardDto.getName())
                .setUser(user)
                .setOrder(userBoardsCount);

        boardRepository.save(newBoard);
    }

    @Transactional
    public void deleteBoard(Integer boardId, User user) {

        List<Board> boards = boardRepository.findAllByUser(user);
        Board board = boards.stream().filter(b->b.getId().equals(boardId)).findFirst().orElse(null);

        if (board == null) {
            throw new OcValidationException("Failed to find Board with id " + boardId);
        }

        if (board.getUser() != user) {
            throw new OcValidationException("This user is not owner of this board");
        }

        for (Task task : board.getTasks()) {
            task.setBoard(null);
            taskRepository.delete(task);
        }

        //posunutí vyšších borderu
        for (Board board1 : boards) {
            if(board1.getOrder()>board.getOrder()){
                board1.setOrder(board1.getOrder()-1);
            }
        }

        boards.remove(board);
        board.setTasks(new HashSet<>());
        boardRepository.save(board);

        user.getBoards().remove(board);
        userRepository.save(user);

        boardRepository.delete(board);
        boardRepository.saveAll(boards);
    }


    public List<BoardDto> getAllBoards(User u) {
        List<Board> boards = boardRepository.findAllByUser(u);

        return boards.stream().map(Board::convertToDto).sorted(Comparator.comparing(BoardDto::getOrder)).collect(Collectors.toList());

    }

    @Transactional
    public void updateBoardOrder(Integer oldIndex, Integer newIndex, User user) {
        List<Board> boards = boardRepository.findAllByUser(user);

        Board movedBoard = boards.stream().filter(b -> b.getOrder().equals(oldIndex)).findFirst().orElse(null);
        if (movedBoard == null) {
            throw new OcValidationException("failed to find board with order " + oldIndex);
        }


        if (oldIndex > newIndex) {
            //posouvám dopředu
            for (Board board : boards) {
                if (board.getOrder() <= oldIndex && board.getOrder() >= newIndex) {
                    board.setOrder(board.getOrder()+1);
                }
            }
        } else {
            //posouvám dozadu

            for (Board board : boards) {
                if (board.getOrder() >= oldIndex && board.getOrder() <= newIndex) {
                    board.setOrder(board.getOrder()-1);
                }
            }
        }
        movedBoard.setOrder(newIndex);

        boardRepository.saveAll(boards);
    }
}
