package cz.miro.diploma.diploma_be.domain.notification;

import cz.miro.diploma.diploma_be.domain.user.User;
import cz.miro.diploma.diploma_be.enums.NOTIFICATION_TYPE;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;


@Entity(name = Notification.TABLE_NAME)

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Accessors(chain = true)
public class Notification {

    public static final String TABLE_NAME = "notification";
    public static final String PARAM_ID = "id";
    public static final String PARAM_HEADER = "header";
    public static final String PARAM_BODY = "body";
    public static final String PARAM_USER= "user";
    public static final String PARAM_TYPE= "type";
    public static final String PARAM_DATE= "date";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PARAM_ID, nullable = false)
    private Integer id;

    @Column(name = PARAM_HEADER)
    private String header;

    @Column(name = PARAM_BODY)
    private String body;

    @Column(name = PARAM_DATE)
    @CreatedDate
    private Date date;

    @Enumerated(EnumType.STRING)
    @Column(name = PARAM_TYPE)
    private NOTIFICATION_TYPE type;

    @ManyToOne
    @JoinColumn(name = PARAM_USER,nullable = false)
    private User user;




}
