--liquibase formatted sql

--changeset brandysky:1

CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NULL, user INT NOT NULL, CONSTRAINT categoryPK PRIMARY KEY (id));

CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, body VARCHAR(255) NULL, date datetime NULL, header VARCHAR(255) NULL, type VARCHAR(255) NULL, user INT NOT NULL, CONSTRAINT notificationPK PRIMARY KEY (id));

CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, body VARCHAR(255) NULL, date_created datetime NULL, done BIT NULL, done_date datetime NULL, due_date datetime NULL, latitude VARCHAR(255) NULL, longitude VARCHAR(255) NULL, name VARCHAR(255) NULL, remind BIT NULL, repeat_interval VARCHAR(255) NULL, use_gps BIT NULL, user INT NOT NULL, CONSTRAINT taskPK PRIMARY KEY (id));

CREATE TABLE task_item (id INT AUTO_INCREMENT NOT NULL, body VARCHAR(255) NULL, date_created datetime NULL, done BIT NULL, done_date datetime NULL, task INT NOT NULL, CONSTRAINT task_itemPK PRIMARY KEY (id));

CREATE TABLE task_style (id INT AUTO_INCREMENT NOT NULL, task INT NULL, CONSTRAINT task_stylePK PRIMARY KEY (id));

CREATE TABLE token (id INT AUTO_INCREMENT NOT NULL, dateCreated datetime NULL, deviceToken VARCHAR(255) NULL, firebaseToken VARCHAR(255) NULL, user_id INT NOT NULL, CONSTRAINT tokenPK PRIMARY KEY (id));

CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, date_registered datetime NULL, email VARCHAR(255) NULL, last_name VARCHAR(255) NULL, name VARCHAR(255) NULL, CONSTRAINT userPK PRIMARY KEY (id));

CREATE TABLE user_activity (id INT AUTO_INCREMENT NOT NULL, date datetime NULL, name VARCHAR(255) NULL, type VARCHAR(255) NULL, user INT NOT NULL, CONSTRAINT user_activityPK PRIMARY KEY (id));

ALTER TABLE task_style ADD CONSTRAINT FK4hrsgygwtlmkdrh9c107s4eah FOREIGN KEY (task) REFERENCES task (id);

ALTER TABLE task ADD CONSTRAINT FK8tnv1cug8b6wqhwvcj0wrw3sb FOREIGN KEY (user) REFERENCES user (id);

ALTER TABLE task_item ADD CONSTRAINT FKb19twlfol0mlcxomwmbyw57l4 FOREIGN KEY (task) REFERENCES task (id);

ALTER TABLE token ADD CONSTRAINT FKe32ek7ixanakfqsdaokm4q9y2 FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE category ADD CONSTRAINT FKnb4ythaqh608feqprd49w39b FOREIGN KEY (user) REFERENCES user (id);

ALTER TABLE user_activity ADD CONSTRAINT FKr553jov6v5m6pbcn3aeyi8rvm FOREIGN KEY (user) REFERENCES user (id);

ALTER TABLE notification ADD CONSTRAINT FKt4xdgxjikls4goj5f66yelly4 FOREIGN KEY (user) REFERENCES user (id);

--changeset miro:1
CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, dateCreated datetime NULL, deviceToken VARCHAR(255) NULL, user_id INT NOT NULL, CONSTRAINT devicePK PRIMARY KEY (id));

ALTER TABLE user ADD login VARCHAR(255) NULL;

ALTER TABLE user ADD password VARCHAR(255) NULL;

ALTER TABLE device ADD CONSTRAINT FKk92m2qj36vn62ctp5pgbt4982 FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE token DROP FOREIGN KEY FKe32ek7ixanakfqsdaokm4q9y2;

DROP TABLE token;

ALTER TABLE user DROP COLUMN email;

--changeset miro:1647710456048-1
ALTER TABLE device ADD date_created datetime NULL;

--changeset miro:1647710456048-2
ALTER TABLE device ADD device_token VARCHAR(255) NULL;

--changeset miro:1647710456048-3
ALTER TABLE device DROP COLUMN dateCreated;

--changeset miro:2
ALTER TABLE device ADD date_refreshed datetime NULL;

--changeset miro:1648049948329-1
ALTER TABLE task ADD category INT NOT NULL;

--changeset miro:1648049948329-2
ALTER TABLE task ADD CONSTRAINT FKlo73s2cwjpmjo6298t3fpylto FOREIGN KEY (category) REFERENCES category (id);

--changeset miro:1648049948329-3
ALTER TABLE user DROP COLUMN last_name;

--changeset miro:1648154843867-1
CREATE TABLE board (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NULL, user INT NOT NULL, CONSTRAINT boardPK PRIMARY KEY (id));

--changeset miro:1648154843867-2
ALTER TABLE task ADD board INT NULL;

--changeset miro:1648154843867-3
ALTER TABLE task ADD CONSTRAINT FK9ak1yvb7kwg9cml9iwvfhpr3i FOREIGN KEY (board) REFERENCES board (id);

--changeset miro:1648154843867-4
ALTER TABLE board ADD CONSTRAINT FKfk1b7b9mgrakgq7uyqrppy7mb FOREIGN KEY (user) REFERENCES user (id);

--changeset miro:1648154843867-5
ALTER TABLE task DROP FOREIGN KEY FK8tnv1cug8b6wqhwvcj0wrw3sb;

--changeset miro:1648154843867-6
ALTER TABLE task DROP FOREIGN KEY FKlo73s2cwjpmjo6298t3fpylto;

--changeset miro:1648154843867-7
ALTER TABLE task DROP COLUMN category;

--changeset miro:1648154843867-8
ALTER TABLE task DROP COLUMN user;

--changeset miro:1648332116344-1
ALTER TABLE board ADD `order` INT NULL;

--changeset miro:1648368271745-1
ALTER TABLE board ADD order_index INT NULL;

--changeset miro:1648368271745-2
ALTER TABLE board DROP COLUMN `order`;

--changeset miro:1648456341061-1
ALTER TABLE task ADD order_index INT NULL;



