import 'package:flutter/cupertino.dart';

class Constant {
  static const String CZ_PHONE_NO_PREFIX = "+420";
  static const String CZ_PHONE_NO_MASK = "000 000 000";
  static const String TOKEN = "jwtToken";
  static const String DEVICE_TOKEN = "deviceToken";
}
