import 'dart:io';

import 'package:app/data/models/user/user_model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class Utils {
  static String dateTimeToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('dd.MM.yyyy HH:mm').format(dateTime);
  }

  static String timeToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('HH:mm').format(dateTime);
  }

  static String dateToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('dd.MM.yyyy').format(dateTime);
  }

  static String dateTimeToJsonString(DateTime dateTime) {
    if (dateTime == null) return "";
    String a = DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
    return a;
  }

  static String dateTimeToInvoiceNameFormat(DateTime dateTime) {
    if (dateTime == null) return "";
    String a = DateFormat('dd-MM-yyyy').format(dateTime);
    return a;
  }

  static DateTime dateTimeFromJson(String dateTime) {
    if (dateTime == null) return null;
    try {
      return DateFormat('yyyy-MM-dd HH:mm').parse(dateTime);
    } catch (e) {
      try {
        return DateFormat('yyyy-MM-ddTHH:mm').parse(dateTime);
      } catch (e) {
        try {
          return DateFormat('yyyy-MM-dd').parse(dateTime);
        } catch (e) {
          return null;
        }
      }
    }
  }


  static Future<bool> isInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (kIsWeb) {
      return true;
    } else {
      if (connectivityResult == ConnectivityResult.mobile) {
        // I am connected to a mobile network, make sure there is actually a net connection.
        if (await DataConnectionChecker().hasConnection) {
          // Mobile data detected & internet connection confirmed.
          return true;
        } else {
          // Mobile data detected but no internet connection found.
          return false;
        }
      } else if (connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a WIFI network, make sure there is actually a net connection.
        if (await DataConnectionChecker().hasConnection) {
          // Wifi detected & internet connection confirmed.
          return true;
        } else {
          // Wifi detected but no internet connection found.
          return false;
        }
      } else {
        // Neither mobile data or WIFI detected, not internet connection found.
        return false;
      }
    }
  }
}
