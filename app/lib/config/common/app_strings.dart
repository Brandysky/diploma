const String DEFAULT_ERROR_MESSAGE = "Something went wrong";
const String DEFAULT_OFFLINE_ERROR_MESSAGE = "You are offline. Turn on your internet connection";
const String DEFAULT_SERVER_DOWN_MESSAGE = "Server is down. Try it again later";

