import 'package:app/config/common/app_strings.dart';

class OcValidationException implements Exception {
  List<ValidationMessage> messages;

  OcValidationException(this.messages);

  OcValidationException.one(ValidationMessage message) {
    this.messages[0] = message;
  }

  OcValidationException.fromJson(Map<String, dynamic> json) {
    List<dynamic> msgs= json['messages'];
    this.messages = msgs.map((m) => ValidationMessage.fromJson(m)).toList();
  }

  String get getFirstMessageOrDefault{
    if(this.messages != null && this.messages.isNotEmpty){
      return this.messages[0].message;
    }
    return DEFAULT_ERROR_MESSAGE;
  }

  String get buildCompleteErrorMessage{
    if(this.messages == null || this.messages.isEmpty){
      return DEFAULT_ERROR_MESSAGE;
    }
    String msg = "";
    for(int i=0;i<this.messages.length;i++){
      msg+= this.messages[i].message;
      if(i<this.messages.length)
        msg+="\n";
    }
    return msg;
  }

}

class ValidationMessage {
  String propertyPath;
  String message;
  String errorCode;

  ValidationMessage({this.propertyPath, this.message, this.errorCode});

  ValidationMessage.fromJson(Map<String, dynamic> json) {
    propertyPath = json['propertyPath'];
    message = json['message'];
    errorCode = json['errorCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['propertyPath'] = this.propertyPath;
    data['message'] = this.message;
    data['errorCode'] = this.errorCode;
    return data;
  }
}
