import 'package:app/config/design/app_colors.dart';
import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: appBarTheme(),
    brightness: Brightness.light,
    textTheme: textTheme(),
    primaryColor: AppColors.PRIMARY_COLOR,
    primaryIconTheme: IconThemeData(color: AppColors.CUSTOM_COLOR_C),
    // cardColor: AppColors.CUSTOM_COLOR_C,
    secondaryHeaderColor: AppColors.SECONDARY_COLOR,
    dialogBackgroundColor: AppColors.BACKGROUND_GREY,
    disabledColor: AppColors.CUSTOM_COLOR_D,

    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: AppColors.CUSTOM_COLOR_C),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.SECONDARY_COLOR)),
    ),

    // errorColor: Colors.red,
    // hintColor: AppColors.BLUE_COLOR,
    cardTheme: CardTheme(color: AppColors.CUSTOM_COLOR_D.withAlpha(100)),
    visualDensity: VisualDensity(horizontal: -4, vertical: -4),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {TargetPlatform.iOS: FadeTransitionBuilder(), TargetPlatform.android: FadeTransitionBuilder()},
    ),
  );
}

extension CustomThemeData on ColorScheme {
  Color get primaryContainer {
    return Colors.green;
  }
}

TextTheme textTheme() {
  /*

  bodyText1 - Hlavní nadpis na stránce

  headline1 - Nadpis 1. úrovně na stránce
  headline2 - headline1 - bold
  headline3 - Nadpis 2. úrovně na stránce
  headline4 - headline3 - bold
  headline5 - Nadpis 3. úrovně na stránce
  headline6 - headline5 - bold

  * */
  return TextTheme(
      bodyText1: TextStyle(color: AppColors.DARK_TEXT_COLOR, fontSize: 25, fontWeight: FontWeight.w900, letterSpacing: 1.3),
      headline1: TextStyle(color: AppColors.DARK_TEXT_COLOR, fontSize: 20, fontWeight: FontWeight.w600, letterSpacing: 1.3),
      headline2: TextStyle(color: AppColors.DARK_TEXT_COLOR, fontSize: 20, fontWeight: FontWeight.w900, letterSpacing: 1.3),
      headline3: TextStyle(color: AppColors.DARK_TEXT_COLOR, fontSize: 16, fontWeight: FontWeight.w600, letterSpacing: 1.3),
      headline4: TextStyle(
        color: AppColors.DARK_TEXT_COLOR,
        fontSize: 16,
        fontWeight: FontWeight.w900,
      ),
      headline5: TextStyle(
        color: AppColors.DARK_TEXT_COLOR,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      headline6: TextStyle(
        color: AppColors.DARK_TEXT_COLOR,
        fontSize: 14,
        fontWeight: FontWeight.w900,
      ),
      caption: TextStyle(
        color: AppColors.DARK_TEXT_COLOR,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      labelMedium: TextStyle(
        color: AppColors.DARK_TEXT_COLOR,
        fontSize: 16,
        fontWeight: FontWeight.w900,
      ));

}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    centerTitle: true,
    color: AppColors.CUSTOM_COLOR_C,
    elevation: 5,
    iconTheme: IconThemeData(color: AppColors.PRIMARY_COLOR),
  );
}

extension CustomColorScheme on ColorScheme {
  Color get success => const Color(0xFF429249);

  Color get info => const Color(0xFF17a2b8);

  Color get warning => const Color(0xFFffc107);

  Color get danger => const Color(0xFFD13933);
}

class FadeTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(_, __, animation, ___, child) => FadeTransition(opacity: AlwaysStoppedAnimation(1), child: child); //0-1 (0= začátek animace, 1 = konec animace)
}
