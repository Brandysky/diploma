
import 'package:flutter/cupertino.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double screenHeightMinusAppBar;
  static Orientation orientation;
  static double statusBarHeight;
  static const double appBarHeight=100;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
    statusBarHeight = _mediaQueryData.padding.top;
    screenHeightMinusAppBar = screenHeight-appBarHeight-statusBarHeight;
  }
}
