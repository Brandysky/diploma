
import 'package:app/presentation/screens/dashboard_screen/dashboard_screen.dart';
import 'package:app/presentation/screens/home/home_screen.dart';
import 'package:app/presentation/screens/loginScreen/login_screen.dart';
import 'package:app/presentation/screens/registerScreen/register_screen.dart';
import 'package:app/presentation/screens/updateTaskScreen/update_task_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class Routes {
  static Map<String, WidgetBuilder> getRoutes() {
    return {
      /*main*/
      HomeScreen.routeName: (context) => HomeScreen(),
      RegisterScreen.routeName: (context) => RegisterScreen(),
      LoginScreen.routeName: (context) => LoginScreen(),
      UpdateTaskScreen.routeName: (context) => UpdateTaskScreen(),
      DashboardScreen.routeName: (context) => DashboardScreen()
    };
  }
}
