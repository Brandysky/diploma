import 'dart:convert';

import 'package:app/data/models/user/user_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class StorageUtil {
  static final FlutterSecureStorage  _storage =new FlutterSecureStorage();


 /* static FlutterSecureStorage getInstance(){
    if (_storage == null) {
      _storage = new FlutterSecureStorage();
    }
    return _storage;
  }*/

// Read value

  static Future<String> read(String key) async {
    return await _storage.read(key: key);
  }

// Read all values
  static Future<Map<String, String>> readAll() async {
    return await _storage.readAll();
  }

// Delete value
  static void delete(String key) async {
    await _storage.delete(key: key);
  }

// Delete all
  static void deleteAll() async {
    await _storage.deleteAll();
  }

// Write value
  static Future<void> write(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  static Future<UserDetail> readUserData() async{

    String value = await _storage.read(key: "user");
    return UserDetail.fromJson(json.decode(value));
  }



  
}
