import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/data/repositories/task_repository.dart';
import 'package:app/data/store/task/create_task/create_task_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/update_task_body.dart';

class UpdateTaskScreenArgs {
  final TaskDto taskDto;
  final int boardId;

  UpdateTaskScreenArgs({@required this.taskDto,@required this.boardId, });
}

class UpdateTaskScreen extends StatefulWidget {
  static String routeName = "/updateTask";

  @override
  _CreateTaskScreenState createState() => _CreateTaskScreenState();
}

class _CreateTaskScreenState extends State<UpdateTaskScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    final arguments =
    ModalRoute.of(context).settings.arguments as UpdateTaskScreenArgs;

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 3,
        leading: BackButton(
            color: Colors.white
        ),
      ),

      body:UpdateTaskBody(arguments.taskDto,arguments.boardId),
    );
  }
}


