import 'package:app/config/common/utils.dart';
import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/data/repositories/authorization_repository.dart';
import 'package:app/data/store/task/board/board_bloc.dart';
import 'package:app/data/store/task/create_task/create_task_bloc.dart';
import 'package:app/presentation/widgets/confirm_dialog.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:app/presentation/widgets/form_info_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/config/common/globals.dart' as globals;

import 'task_item.dart';

class UpdateTaskBody extends StatefulWidget {
  final TaskDto taskDto;
  final int boardId;

  UpdateTaskBody(this.taskDto, this.boardId);

  @override
  _UpdateTaskBodyState createState() => _UpdateTaskBodyState();
}

class _UpdateTaskBodyState extends State<UpdateTaskBody> {
  TaskDto taskDto;
  bool dueDatePicked = false;
  bool _inputIsValid = false;

  String _errorMessage = "";

  final nameController = TextEditingController();
  final bodyController = TextEditingController();
  final dueDateController = TextEditingController();

  final scrollController = ScrollController();

  // final locationController = TextEditingController();
  final remindController = TextEditingController();
  final repeatController = TextEditingController();

  FocusNode focusNode;

  @override
  void initState() {
    initTask();
    super.initState();
  }

  initTask() {
    taskDto = widget.taskDto;
    nameController.text = taskDto.name;
    bodyController.text = taskDto.body;
    dueDateController.text = Utils.dateTimeToUserReadString(taskDto.dueDate);
    inputChanged();
  }

  Future<void> _selectDueDate(BuildContext context) async {
    DateTime initialDateTime = taskDto.dueDate != null ? taskDto.dueDate : new DateTime.now().add(Duration(days: 1));

    final DateTime date = await showDatePicker(context: context, initialDate: initialDateTime, firstDate: new DateTime.now().add(Duration(days: -7)), lastDate: DateTime(2101));

    final TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: initialDateTime.hour, minute: initialDateTime.minute),
    );

    DateTime chosenDateWithTime = DateTime(date.year, date.month, date.day, time.hour, time.minute);

    if (chosenDateWithTime != null && chosenDateWithTime != taskDto.dueDate) {
      setState(() {
        dueDateController.text = Utils.dateTimeToUserReadString(chosenDateWithTime);
        taskDto.dueDate = chosenDateWithTime;
        dueDatePicked = true;
      });

      inputChanged();
    }
  }

  void inputChanged() {
    bool val = true;
    if (taskDto.name == null || taskDto.name.isEmpty || taskDto.name.length < 1) val = false;

    setState(() {
      _inputIsValid = val;
    });
  }

  void updateTask() {
    if (!dueDatePicked) {
      taskDto.dueDate = null;
    }
    BlocProvider.of<CreateTaskBloc>(context).add(UpdateTaskEvent(taskDto, widget.boardId));
  }

  void setName(String name) {
    setState(() {
      taskDto.name = name;
    });
    inputChanged();
  }

  void setBody(String body) {
    setState(() {
      taskDto.body = body;
    });
    inputChanged();
  }

  void addSubTask() {
    FocusNode newFocusNode = FocusNode();
    setState(() {
      taskDto.items.add(TaskItemDto(done: false, focusNode: newFocusNode, key: UniqueKey()));
      newFocusNode.requestFocus();
    });

    double size = 100.0 * taskDto.items.length;
    scrollController.animateTo(size, duration: Duration(seconds: 1), curve: Curves.ease);
  }

  void clear() {
    /*nameController.clear();
    bodyController.clear();
    dueDateController.clear();
    setState(() {

      taskDto = TaskDto();
    });*/
  }

  void removeTaskItem(TaskItemDto taskItemDto) {
    print(taskItemDto.toJson());
    setState(() {
      List<TaskItemDto> items = taskDto.items;
      items.remove(taskItemDto);
      taskDto.items = items;
      print(taskDto.items);
    });
  }

  createSubTaskList() {
    List<TaskItemWidget> defaultList = [];
    if (taskDto != null || taskDto.items != null) {
      defaultList = [
        for (var item in taskDto.items) new TaskItemWidget(item, removeTaskItem, UniqueKey(), item.focusNode),
      ];
    }
    // defaultList.add(TaskItemWidget(TaskItemDto(done: false), () => {}));

    return defaultList;
  }

  showConfirmDialog() {
    return (BuildContext context) => ConfirmDialog(() => {
          BlocProvider.of<CreateTaskBloc>(context).add(DeleteTaskEvent(taskDto.id)),
          Navigator.of(context).pop(),
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: (globals.screenWidth == globals.ScreenWidth.LARGE || globals.screenWidth == globals.ScreenWidth.MEDIUM || globals.screenWidth == globals.ScreenWidth.X_LARGE) ? Align(alignment: Alignment.topCenter, child: _buildForm()) : _buildForm()),
    );
  }

  Widget _buildForm() {
    return Shortcuts(
      shortcuts: {
        LogicalKeySet(LogicalKeyboardKey.delete): DeleteTaskIntent(),
      },
      child: Actions(
        actions: <Type, Action<Intent>>{
          DeleteTaskIntent: DeleteTaskAction(taskDto.id, context),
        },
        child: Builder(
            builder: (BuildContext context) => Focus(
                  autofocus: true,
                  child: _buildFormContent(),
                )),
      ),
    );
  }

  Widget _buildFormContent() {
    return Container(
      width: (globals.screenWidth == globals.ScreenWidth.SMALL || globals.screenWidth == globals.ScreenWidth.MEDIUM)
          ? double.infinity
          : (globals.screenWidth == globals.ScreenWidth.LARGE)
              ? SizeConfig.screenWidth * .5
              : 500,
      padding: EdgeInsets.symmetric(horizontal: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 0 : 20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            /* SizedBox(height: height * .05),
                RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    children: [
                      TextSpan(
                        text: 'Create new task',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 30,
                        ),
                      ),
                    ],
                  ),
                ),*/
            SizedBox(
              height: 10,
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.drive_file_rename_outline,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                title: new TextField(
                  maxLines: 1,
                  controller: nameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setName(value),
                  decoration: new InputDecoration(contentPadding: EdgeInsets.all(5.0), isDense: false, labelText: "Topic", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.subtitles_outlined,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: AppColors.SECONDARY_COLOR,
                ),
                title: new TextField(
                  maxLines: 1,
                  controller: bodyController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setBody(value),
                  decoration: new InputDecoration(contentPadding: EdgeInsets.all(5.0), isDense: false, labelText: "Body", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.event_available_outlined,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: AppColors.SECONDARY_COLOR,
                ),
                title: new TextField(
                  controller: dueDateController,
                  onTap: () {
                    _selectDueDate(context);
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  decoration: new InputDecoration(
                    labelText: "due date",
                    contentPadding: EdgeInsets.all(5.0),
                    isDense: false,
                    hintText: Utils.dateTimeToUserReadString(taskDto.dueDate),
                  ),
                ),
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 5, 5),
                  child: Container(
                    child: Text("Subtasks", style: Theme.of(context).textTheme.headline3),
                  ),
                ),
                Tooltip(

                  message: "Add subtask",
                  child: MaterialButton(
                    onPressed: () {
                      addSubTask();
                    },
                    elevation: 0,
                    focusElevation: 0,
                    hoverElevation: 0,
                    textColor: AppColors.CUSTOM_COLOR_E,
                    child: Icon(
                      Icons.add_box_outlined,
                      size: 24,
                    ),
                    padding: EdgeInsets.all(15),
                    shape: CircleBorder(),
                  ),
                )
              ],
            ),
            SizedBox(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: createSubTaskList(),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            BlocBuilder<CreateTaskBloc, CreateTaskState>(builder: (context, state) {
              String message = "";
              bool error = false;
              if (state is CreateTaskSaving) {
                message = "Saving...";
              } else if (state is CreateTaskSaved) {
                message = "Saved";
              } else if (state is CreateTaskError) {
                message = state.errorMessage;
                error = true;
              }
              return FormInfoLabel(message, SizeConfig.screenHeight * .05, error);
            }),
            (globals.screenWidth == globals.ScreenWidth.SMALL)
                ? Column(
                    children: [
                      _buildSaveBtn(),
                      SizedBox(
                        height: 10,
                      ),
                      _buildDeleteBtn()
                    ],
                  )
                : Row(
                    children: [
                      _buildSaveBtn(),
                      SizedBox(
                        width: 10,
                      ),
                      _buildDeleteBtn()
                    ],
                  ),
            SizedBox(
              height: 30,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildDeleteBtn() {
    return DefaultButton(
      width: 100,
      height: 50,
      color: Theme.of(context).colorScheme.error,
      text: "Delete",
      press: () => {
        showDialog<String>(
          context: context,
          builder: showConfirmDialog(),
        )
      },
    );
  }

  Widget _buildSaveBtn() {
    return BlocConsumer<CreateTaskBloc, CreateTaskState>(
      listener: (context, state) {
        _errorMessage = "";
        if (state is CreateTaskSaving) {
        } else if (state is CreateTaskSaved) {
          Navigator.of(context).pop();
          clear();
        } else if (state is CreateTaskError) {
          setState(() {
            _errorMessage = state.errorMessage;
          });
        }
      },
      builder: (context, state) {
        return DefaultButton(
          width: (globals.screenWidth == globals.ScreenWidth.X_LARGE) ? 200 : 100,
          text: "Save",
          isLoading: state is CreateTaskSaving,
          isEnabled: _inputIsValid,
          press: () {
            updateTask();
          },
        );
      },
    );
  }
}

class DeleteTaskAction extends Action<DeleteTaskIntent> {
  DeleteTaskAction(this.taskId, this.context);

  final BuildContext context;
  final int taskId;

  showConfirmDialog(int taskId) {
    return (BuildContext context) => ConfirmDialog(() => {
          BlocProvider.of<CreateTaskBloc>(context).add(DeleteTaskEvent(taskId)),
          Navigator.of(context).pop(),
        });
  }

  @override
  void invoke(covariant DeleteTaskIntent intent) => {
        showDialog<String>(
          context: context,
          builder: showConfirmDialog(taskId),
        )
      };
}

class DeleteTaskIntent extends Intent {
  const DeleteTaskIntent();
}
