import 'dart:ui';

import 'package:app/data/models/task/TaskDto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TaskItemWidget extends StatefulWidget {
  final TaskItemDto taskItem;
  final Function removeTaskItem;
  final UniqueKey key;
  final FocusNode focusNode;

  TaskItemWidget(this.taskItem, this.removeTaskItem, this.key, this.focusNode);

  @override
  State<TaskItemWidget> createState() => _TaskItemWidgetState();
}

class _TaskItemWidgetState extends State<TaskItemWidget> {
  final bodyController = TextEditingController();

  @override
  void initState() {
    initTaskItem();
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  void reassemble() {
    print("__");
    super.reassemble();
  }

  initTaskItem() {
    if (widget.taskItem.done == null) {
      setState(() {
        widget.taskItem.done = false;
      });
    }
    bodyController.text = widget.taskItem.body;
  }

  void setBody(String body) {
    setState(() {
      widget.taskItem.body = body;
    });
  }

  @override
  Widget build(BuildContext context) {


    return Row(
      children: [
        Flexible(
          child: SizedBox(
            height: 50,
            child: CheckboxListTile(
              contentPadding: EdgeInsets.zero,

              title: TextField(
                maxLines: 1,
                focusNode: widget.focusNode,
                controller: bodyController,
                inputFormatters: [LengthLimitingTextInputFormatter(255)],
                onChanged: (value) => setBody(value),
                decoration: new InputDecoration(contentPadding: EdgeInsets.all(5.0), isDense: false,  hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
              ),
              value: widget.taskItem.done,
              onChanged: (newValue) {
                setState(() {
                  widget.taskItem.done = newValue;
                });
              },
              controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
            ),
          ),
        ),
        SizedBox(
          width: 50,
          child: MaterialButton(
            onPressed: () {
              widget.removeTaskItem(widget.taskItem);
            },
            textColor: Colors.white,
            child: Icon(
              Icons.delete_forever_outlined,
              color: Colors.black,
              size: 24,
            ),
            shape: CircleBorder(),
          ),
        ),
      ],
    );
    ;
  }
}
