import 'package:app/config/common/app_strings.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/store/app/connection/connection_bloc.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OfflineScreen extends StatefulWidget{

  @override
  _OfflineScreenState createState() => _OfflineScreenState();
}

class _OfflineScreenState extends State<OfflineScreen> {



  @override
  void initState() {
    super.initState();


    wait();
  }

  Future<void> wait() async{
    await Future.delayed(Duration(milliseconds: 2000));

  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.wifi_off_rounded,
            size: SizeConfig.screenWidth * 0.22,
          ),
          SizedBox(
            width: SizeConfig.screenWidth * 0.85,
            child: Text(DEFAULT_OFFLINE_ERROR_MESSAGE,
                style: Theme.of(context).textTheme.headline1,
                softWrap: true,
                textAlign: TextAlign.center),
          ),
          DefaultButton(text: "Načíst znovu", width: 200, press: () {
            BlocProvider.of<ConnectionBloc>(context).add(CheckConnectionEvent());
          })
        ],
      ),
    );
  }
}