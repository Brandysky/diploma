import 'package:app/config/common/app_strings.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/store/app/connection/connection_bloc.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ServerDownScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.report_gmailerrorred_rounded,
            size: SizeConfig.screenWidth * 0.22,
          ),
          SizedBox(
            width: SizeConfig.screenWidth * 0.85,
            child: Text(DEFAULT_SERVER_DOWN_MESSAGE,
                style: Theme.of(context).textTheme.headline1,
                softWrap: true,
                textAlign: TextAlign.center),
          ),
          Padding(
            padding: const EdgeInsets.only(top:20.0,bottom: 20),
            child: DefaultButton(
                text: "Try it again",
                width: 200,
                press: () {
                  BlocProvider.of<ConnectionBloc>(context)
                      .add(CheckConnectionEvent());
                }),
          ),
        ],
      ),
    );
  }
}
