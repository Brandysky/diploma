import 'package:app/config/design/app_colors.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomLeft,
            colors: [
              AppColors.PRIMARY_COLOR,
              AppColors.SECONDARY_COLOR,
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.today_outlined,
              color: Colors.black,
              size: 80,
            ),
            Text("Simple Plan",style: TextStyle(fontSize: 20,fontWeight:FontWeight.w700  ))
          ],
        ),
      ),
    );
  }
}
