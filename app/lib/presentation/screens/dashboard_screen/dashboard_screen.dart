import 'package:app/config/design/size_config.dart';
import 'package:app/presentation/screens/home/widgets/navigation_drawer.dart';
import 'package:flutter/material.dart';

import 'widgets/dashboard_body.dart';
import 'package:app/config/common/globals.dart' as globals;

class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class DashboardScreen extends StatefulWidget {
  static String routeName = "/Dashboard";

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (globals.screenWidth == globals.ScreenWidth.SMALL || globals.screenWidth == globals.ScreenWidth.MEDIUM || globals.screenWidth == globals.ScreenWidth.LARGE) {
      return Scaffold(
        key: _drawerKey,
        drawer: NavigationDrawer(),
        drawerScrimColor: Theme.of(context).primaryColor.withAlpha(50),
        resizeToAvoidBottomInset: false,
        appBar: (globals.screenWidth == globals.ScreenWidth.SMALL)
            ? AppBar(
                leading: Container(),
                leadingWidth: 0,
                title: Center(
                  child: IconButton(
                      icon: const Icon(Icons.menu),
                    onPressed: ()=> {_drawerKey.currentState.openDrawer(), print("123")},
                  ),
                ),
                titleSpacing: -30,
                centerTitle: false,
                elevation: 3,
              )
            : AppBar(
                titleSpacing: -30,
                centerTitle: false,
                elevation: 3,
              ),
        body: DashboardBody(),
      );
    } else {
      return Scaffold(
        key: _drawerKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: Container(),
          elevation: 3,
        ),
        body: Row(
          children: [
            SizedBox(width: 200, child: NavigationDrawer()),
            Flexible(child: DashboardBody()),
          ],
        ),
      );
    }
  }
}
