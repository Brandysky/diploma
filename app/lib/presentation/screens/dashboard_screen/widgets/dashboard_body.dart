import 'package:app/config/common/styles.dart';
import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

import 'custom_chart.dart';
import 'stats_grid.dart';
import 'package:app/config/common/globals.dart' as globals;

class DashboardBody extends StatefulWidget {
  @override
  _DashboardBodyState createState() => _DashboardBodyState();
}

class _DashboardBodyState extends State<DashboardBody> {
  DashboardPeriod selectedPeriod = DashboardPeriod.DAILY;

  @override
  void initState() {
    super.initState();
  }

  final values = [12.17, 11.15, 10.02, 11.21, 13.83, 14.16, 20.30];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: (globals.screenWidth == globals.ScreenWidth.LARGE || globals.screenWidth == globals.ScreenWidth.MEDIUM || globals.screenWidth == globals.ScreenWidth.X_LARGE) ? Align(alignment: Alignment.topCenter, child: _buildContent()) : _buildContent()),
    );
  }


  Widget _buildContent() {
    return Container(
      height: SizeConfig.screenHeightMinusAppBar,
      width: (globals.screenWidth == globals.ScreenWidth.SMALL || globals.screenWidth == globals.ScreenWidth.MEDIUM)
          ? double.infinity
          : (globals.screenWidth == globals.ScreenWidth.LARGE)
              ? SizeConfig.screenWidth
              : SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 0 : 20),
      child: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: (globals.screenWidth == globals.ScreenWidth.X_LARGE)
              ? Row(
            crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: SizeConfig.screenWidth * .5,
                      child: _buildStatsComponent(),
                    ),
                    Container(
                      width: SizeConfig.screenWidth * .2,
                      child: _buildChartComponent(),
                    ),
                  ],
                )
              : Column(
                  children: [
                    Container( child: _buildStatsComponent()),
                    Container(
                      child: _buildChartComponent(),
                    ),
                  ],
                )),
    );
  }

  Widget _buildStatsComponent() {
    return Column(
      children: [
        _buildHeader(),
        _buildStatsTabBar(),
        SizedBox(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: StatsGrid(selectedPeriod),
          ),
        )
      ],
    );
  }

  Widget _buildChartComponent() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: CustomChart(covidCases: values),
        ),
        SizedBox(
          height: 100,
        )
      ],
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Text(
        'Dashboard',
        style: const TextStyle(
          color: Colors.black,
          fontSize: 25.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _buildRegionTabBar() {
    return DefaultTabController(
      length: 2,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20.0),
        height: 50.0,
        decoration: BoxDecoration(
          color: Theme.of(context).secondaryHeaderColor,
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: TabBar(
          indicator: BubbleTabIndicator(
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
            indicatorHeight: 40.0,
            indicatorColor: Colors.white,
          ),
          labelStyle: Styles.tabTextStyle,
          labelColor: Colors.black,
          unselectedLabelColor: Colors.white,
          tabs: <Widget>[
            Text('My Country'),
            Text('Global'),
          ],
          onTap: (index) {},
        ),
      ),
    );
  }

  Widget _buildBarItem(String label) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: FittedBox(
          fit: BoxFit.none,child: Text(label)),
    );
  }

  Widget _buildStatsTabBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DefaultTabController(
        length: 4,
        child: TabBar(
          indicator: BoxDecoration(
            color: AppColors.SECONDARY_COLOR,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          indicatorColor: Colors.transparent,
          labelStyle: Styles.tabTextStyle,

          unselectedLabelColor: Theme.of(context).disabledColor,
          tabs: <Widget>[
            _buildBarItem('Today'),
            _buildBarItem('Week'),
            _buildBarItem('Month'),
            _buildBarItem('Total'),
          ],
          onTap: (index) {
            print(index);
            switch (index) {
              case 0:
                selectedPeriod = DashboardPeriod.DAILY;
                break;
              case 1:
                selectedPeriod = DashboardPeriod.WEEKLY;
                break;
              case 2:
                selectedPeriod = DashboardPeriod.MONTHLY;
                break;
              case 3:
                selectedPeriod = DashboardPeriod.TOTAL;
                break;
            }
            setState(() {});
          },
        ),
      ),
    );
  }
}
