import 'package:app/config/design/size_config.dart';
import 'package:flutter/material.dart';
import 'package:app/config/common/globals.dart' as globals;

enum DashboardPeriod {
  DAILY,
  WEEKLY,
  MONTHLY,
  TOTAL,
}

class StatsGrid extends StatelessWidget {
  final DashboardPeriod period;

  StatsGrid(this.period);

  final Map<DashboardPeriod, Map<String, String>> total_data = {
    DashboardPeriod.DAILY: {
      "total_tasks": '5',
      "tasks_done": '2',
      "tasks_failed": '3',
      "total_boards": '3',
      "total_subtasks": '4',
    },
    DashboardPeriod.WEEKLY: {
      "total_tasks": '53',
      "tasks_done": '48',
      "tasks_failed": '5',
      "total_boards": '4',
      "total_subtasks": '34',
    },
    DashboardPeriod.MONTHLY: {
      "total_tasks": '201',
      "tasks_done": '150',
      "tasks_failed": '51',
      "total_boards": '6',
      "total_subtasks": '174',
    },
    DashboardPeriod.TOTAL: {
      "total_tasks": '533',
      "tasks_done": '499',
      "tasks_failed": '34',
      "total_boards": '8',
      "total_subtasks": '344',
    },
  };

  @override
  Widget build(BuildContext context) {
    List<Widget> itemsA = [
      _buildStatCard('Total tasks', total_data[period]["total_tasks"], Colors.lightGreen),
      _buildStatCard('Tasks done', total_data[period]["tasks_done"], Colors.lightBlueAccent),
    ];

    List<Widget> itemsB = [_buildStatCard('Tasks failed', total_data[period]["tasks_failed"], Colors.red), _buildStatCard('Total boards', total_data[period]["total_boards"], Colors.amber), _buildStatCard('Total subtasks', total_data[period]["total_subtasks"], Colors.deepPurple)];

    List<Widget> itemsAB = itemsA + itemsB;

    return (globals.screenWidth == globals.ScreenWidth.X_LARGE)
        ? _buildRow(itemsAB)
        : (globals.screenWidth == globals.ScreenWidth.SMALL)
            ? _buildColumnForSmall(itemsAB)
            : _buildColumns(itemsA, itemsB);
  }

  Widget _buildColumns(List<Widget> itemsA, List<Widget> itemsB) {
    return Container(
      height: SizeConfig.screenHeightMinusAppBar * 0.25,
      child: Column(
        children: <Widget>[
          Flexible(
            child: Row(
              children: itemsA,
            ),
          ),
          Flexible(
            child: Row(
              children: itemsB,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRow(List<Widget> itemsAB) {
    return Container(
      child: Row(
        children: <Widget>[
          Flexible(
            child: Row(
              children: itemsAB,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildColumnForSmall(List<Widget> itemsAB) {
    return Container(
      height:350,
      child: Container(
        width: SizeConfig.screenWidth,
        child: Column(
          children: itemsAB,
        ),
      ),
    );
  }

  Widget _buildStatCard(String title, String count, Color color) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(4.0),
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                title,
                softWrap: true,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                count,
                softWrap: true,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
