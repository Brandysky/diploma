import 'package:app/config/design/size_config.dart';
import 'package:flutter/material.dart';

import 'widgets/login_body.dart';

class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class LoginScreen extends StatefulWidget {
  static String routeName = "/Login";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,

      body: LoginBody(),
    );
  }
}
