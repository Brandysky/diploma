import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/user/login_request.dart';
import 'package:app/data/models/user/signup_request.dart';
import 'package:app/data/store/auth/login/auth_bloc.dart';
import 'package:app/presentation/screens/home/home_screen.dart';
import 'package:app/presentation/screens/registerScreen/register_screen.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:app/presentation/widgets/form_info_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/config/common/globals.dart' as globals;

class LoginBody extends StatefulWidget {
  @override
  _LoginBodyState createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  final loginController = TextEditingController();
  final passwordController = TextEditingController();
  bool _inputIsValid = false;
  String _errorMessage = "";
  LoginRequest loginRequest = LoginRequest();

  @override
  void initState() {
    loginRequest = LoginRequest();
    super.initState();
  }

  @override
  void dispose() {
    loginController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void setLogin(String login) {
    setState(() {
      loginRequest.login = login;
    });
    inputChanged();
  }

  void setPassword(String password) {
    setState(() {
      loginRequest.password = password;
    });
    inputChanged();
  }

  void setName(String name) {
    setState(() {
      loginRequest.name = name;
    });
    inputChanged();
  }

  bool isPasswordCompliant(String password) {
    if (password == null || password.isEmpty) {
      return false;
    }

    bool hasMinLength = password.length >= 8;

    return hasMinLength;
  }

  void inputChanged() {
    bool val = true;
    if (loginRequest.password == null || loginRequest.password.isEmpty) val = false;
    if (loginRequest.login == null || loginRequest.login.isEmpty) val = false;

    if (loginRequest.login == null || loginRequest.login == "" || loginRequest.login.length < 3) val = false;

    if (loginRequest.password == null || loginRequest.password == "" || !isPasswordCompliant(loginRequest.password)) val = false;

    setState(() {
      _inputIsValid = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: (globals.screenWidth == globals.ScreenWidth.LARGE || globals.screenWidth == globals.ScreenWidth.MEDIUM || globals.screenWidth == globals.ScreenWidth.X_LARGE) ? Center(child: _buildLoginForm()) : _buildLoginForm()),
    );
  }

  Widget _buildLoginForm() {
    return Container(
      width: (globals.screenWidth == globals.ScreenWidth.SMALL || globals.screenWidth == globals.ScreenWidth.MEDIUM)
          ? double.infinity
          : (globals.screenWidth == globals.ScreenWidth.LARGE)
              ? SizeConfig.screenWidth * .5
              : 500,
      padding: EdgeInsets.symmetric(horizontal: (globals.screenWidth == globals.ScreenWidth.SMALL)?0:20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20 * .05),
            RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                children: [
                  TextSpan(
                    text: 'Login',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.badge_rounded,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                title: new TextField(
                  maxLines: 1,
                  controller: loginController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setLogin(value),
                  decoration: new InputDecoration(labelText: "Login", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.lock_rounded,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                title: new TextField(
                  obscureText: true,
                  maxLines: 1,
                  controller: passwordController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setPassword(value),
                  decoration: new InputDecoration(labelText: "Password", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),
            BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
              String message = "";
              bool error = false;
              if (state is AuthEvent) {
              } else if (state is AuthLoadedState) {
              } else if (state is AuthErrorState) {
                message = state.errorMessage;
                error = true;
              }
              return FormInfoLabel(message, SizeConfig.screenHeight * .05, error);
            }),
            BlocConsumer<AuthBloc, AuthState>(
              listener: (context, state) {
                _errorMessage = "";
                if (state is AuthLoadingState) {
                } else if (state is AuthLoadedState) {
                  print("SAVED");
                  Navigator.of(context).pushNamed(HomeScreen.routeName);
                } else if (state is AuthErrorState) {
                  setState(() {
                    _errorMessage = state.errorMessage;
                  });
                }
              },
              builder: (context, state) {
                return DefaultButton(
                  width: SizeConfig.screenWidth * .85 * .7,
                  text: "Log in",
                  isLoading: state is AuthLoadingState,
                  isEnabled: _inputIsValid,
                  press: () {
                    BlocProvider.of<AuthBloc>(context).add(LoginRequestEvent(loginRequest));
                  },
                );
              },
            ),
            SizedBox(
              height: 30,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(RegisterScreen.routeName);
              },
              child: Container(
                margin: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                padding: EdgeInsets.all(15),
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        'You don\'t have an account ?',
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Create it',
                      style: TextStyle(
                        color: AppColors.CUSTOM_COLOR_E,
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
