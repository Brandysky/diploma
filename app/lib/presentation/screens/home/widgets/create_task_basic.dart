import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/data/store/task/board/board_bloc.dart';
import 'package:app/data/store/task/create_board/create_board_bloc.dart';
import 'package:app/data/store/task/create_task/create_task_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateTaskBasic extends StatefulWidget {
  final int boardId;

  CreateTaskBasic(this.boardId);

  @override
  State<CreateTaskBasic> createState() => _CreateTaskBasicState();
}

class _CreateTaskBasicState extends State<CreateTaskBasic> {
  final nameController = TextEditingController();
  TaskDto taskDto = TaskDto();
  bool _inputIsValid = false;

  void clear() {
    nameController.clear();
    setState(() {
      _inputIsValid = false;
      taskDto = TaskDto();
    });
  }

  void setName(String name) {
    setState(() {
      taskDto.name = name;
    });
    inputChanged();
  }

  void inputChanged() {
    bool val = true;
    if (taskDto.name == null || taskDto.name.isEmpty || taskDto.name.length < 1) val = false;

    setState(() {
      _inputIsValid = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.move,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            // color: Theme.of(context).primaryColor.withAlpha(100),
          ),
          // padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 0
                  ),
                  child: new ListTile(
                    dense:true,
                    contentPadding: EdgeInsets.only(left: 5,right: 5),
                    title: new TextField(
                      maxLines: 1,
                      controller: nameController,
                      inputFormatters: [LengthLimitingTextInputFormatter(255)],
                      onChanged: (value) => setName(value),
                      decoration: new InputDecoration(labelText: "New task", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary),),
                    ),
                  ),
                ),
              ),
              BlocConsumer<CreateTaskBloc, CreateTaskState>(
                listener: (context, state) {
                  if (state is CreateTaskSaved) {
                    clear();
                    BlocProvider.of<BoardBloc>(context).add(BoardLoadEvent());
                  }
                },
                builder: (context, state) {
                  return  SizedBox(
                    width: 50,
                    child: Tooltip(
                      message: "Add task",
                      child: MaterialButton(
                        onPressed: () {
                          if (_inputIsValid) {BlocProvider.of<CreateTaskBloc>(context).add(CreateNewTaskEvent(taskDto, widget.boardId));}

                        },
                        color: (_inputIsValid) ?AppColors.CUSTOM_COLOR_E.withAlpha(50):Theme.of(context).disabledColor.withAlpha(50),
                        elevation: 0,
                        focusElevation: 0,
                        hoverElevation: 0,
                        textColor: (_inputIsValid)?AppColors.CUSTOM_COLOR_E:Theme.of(context).disabledColor,
                        child:(state is CreateTaskSaving)
                            ? CircularProgressIndicator(backgroundColor: Colors.white): Icon(
                          Icons.add_box_outlined,
                          size: 24,
                        ),
                        padding: EdgeInsets.all(5),
                        shape: CircleBorder(),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
