import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/store/task/board/board_bloc.dart';
import 'package:app/data/store/task/create_board/create_board_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateBoard extends StatefulWidget {
  @override
  State<CreateBoard> createState() => _CreateBoardState();
}

class _CreateBoardState extends State<CreateBoard> {
  BoardDto board = new BoardDto();
  bool _inputIsValid = false;

  final nameController = TextEditingController();

  void setName(String name) {
    setState(() {
      board.name = name;
    });
    inputChanged();
  }

  void inputChanged() {
    bool val = true;
    if (board.name == null || board.name.isEmpty || board.name.length < 1) val = false;

    setState(() {
      _inputIsValid = val;
    });
  }

  void clear() {
    nameController.clear();
    setState(() {
      _inputIsValid = false;
      board = BoardDto();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.only(left: 5, right: 5),
                title: new TextField(
                  maxLines: 1,
                  controller: nameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setName(value),
                  decoration: new InputDecoration(labelText: "New board", hintStyle: TextStyle(color: Theme
                      .of(context)
                      .colorScheme
                      .primary)),
                ),
              ),
            ),
          ),
          BlocConsumer<CreateBoardBloc, CreateBoardState>(
            listener: (context, state) {
              if (state is CreateBoardSaved) {
                clear();
                BlocProvider.of<BoardBloc>(context).add(BoardLoadEvent());
              }
            },
            builder: (context, state) {
              return
                SizedBox(
                    width: 50,
                    child: MaterialButton(
                      onPressed: () {
                        if (_inputIsValid) {
                          BlocProvider.of<CreateBoardBloc>(context).add(CreateNewBoardEvent(board));
                        }
                      },
                      color: (_inputIsValid) ? AppColors.CUSTOM_COLOR_E.withAlpha(50) : Theme
                          .of(context)
                          .disabledColor
                          .withAlpha(50),
                      elevation: 0,
                      focusElevation: 0,
                      hoverElevation: 0,
                      textColor: (_inputIsValid) ? AppColors.CUSTOM_COLOR_E : Theme
                          .of(context)
                          .disabledColor,
                      child: (state is CreateBoardSaving)
                          ? CircularProgressIndicator(backgroundColor: Colors.white) : Icon(
                        Icons.add_box_outlined,
                        size: 24,
                      ),
                      padding: EdgeInsets.all(5),
                      shape: CircleBorder
                        (
                      )
                      ,
                    ));
            },
          ),
        ],
      ),
    );
  }
}
