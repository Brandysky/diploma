import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/data/store/task/board/board_bloc.dart';
import 'package:app/data/store/task/delete_board/delete_board_bloc.dart';
import 'package:app/presentation/screens/updateTaskScreen/update_task_screen.dart';
import 'package:app/presentation/widgets/confirm_dialog.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'create_board.dart';
import 'create_task_basic.dart';
import 'package:app/config/common/globals.dart' as globals;

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class InnerList {
  final String name;
  List<String> children;

  InnerList({@required this.name, @required this.children});
}

class _HomeBodyState extends State<HomeBody> {
  List<BoardDto> _list = [];
  String _errorMessage = "";

  @override
  void initState() {
    super.initState();

    BlocProvider.of<BoardBloc>(context).add(BoardLoadEvent());
  }

  void addSubTask() {}

  deleteBoard() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<BoardBloc, BoardState>(
        listener: (context, state) {
          _errorMessage = "";
          if (state is BoardLoading) {
          } else if (state is BoardLoaded) {
            setState(() {
              _list = state.boards;
            });
          } else if (state is BoardErrorState) {
            setState(() {
              _errorMessage = state.errorMessage;
            });
          }
        },
        builder: (context, state) {
          if (state is BoardErrorState) {
            return Center(
              child: Text(state.errorMessage),
            );
          }
          return ScrollConfiguration(
            behavior: ScrollConfiguration.of(context).copyWith(
            dragDevices: {
              PointerDeviceKind.mouse,
              PointerDeviceKind.touch,
            },
          ),
            child: DragAndDropLists(
              children: createBoardListWithCreateBoard(),
              onItemReorder: _onItemReorder,
              onListReorder: _onListReorder,
              contentsWhenEmpty: Text(""),
              axis: Axis.horizontal,
              listWidth: (globals.screenWidth == globals.ScreenWidth.SMALL) ? SizeConfig.screenWidth * .9 : 250,
              listDraggingWidth: (globals.screenWidth == globals.ScreenWidth.SMALL) ? SizeConfig.screenWidth * .9 : 250,
              listDecoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.all(Radius.circular(7.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black45,
                    spreadRadius: 3.0,
                    blurRadius: 6.0,
                    offset: Offset(2, 3),
                  ),
                ],
              ),
              listPadding: EdgeInsets.all(8.0),
            ),
          );
        },
      ),
    );
  }

  createBoardListWithCreateBoard() {
    List<DragAndDropList> defaultList = List.generate(_list.length, (index) => _buildList(_list[index]));
    defaultList.add(_buildNewBoardForm());

    return defaultList;
  }

  DragAndDropList _buildNewBoardForm() {
    return DragAndDropList(
      canDrag: false,
      contentsWhenEmpty: Container(),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(7.0)),
        color: Theme.of(context).secondaryHeaderColor,
      ),
      header: CreateBoard(),
      children: [],
    );
  }

  showConfirmDialog(int boardid) {
    return (BuildContext context) => ConfirmDialog(() => {
          BlocProvider.of<DeleteBoardBloc>(context).add(DeleteNewBoardEvent(boardid)),
        });
  }

  _buildList(BoardDto board) {
    List<TaskDto> tasks = board.tasks;
    return DragAndDropList(
      decoration: BoxDecoration(
        color: Theme.of(context).secondaryHeaderColor,
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      contentsWhenEmpty: Container(),
      header: MouseRegion(
        cursor: SystemMouseCursors.move,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(7.0)),
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                padding: EdgeInsets.all(5),
                child: Row(
                  children: [
                    Text(
                      board.name,
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                    Expanded(child: Container()),
                    BlocConsumer<DeleteBoardBloc, DeleteBoardState>(
                      listener: (context, state) {
                        if (state is DeleteBoardDeleting) {
                        } else if (state is DeleteBoardDeleted) {
                          BlocProvider.of<BoardBloc>(context).add(BoardLoadEvent());
                        }
                      },
                      builder: (context, state) {
                        return MouseRegion(
                          cursor: SystemMouseCursors.click,
                          child: Tooltip(
                            message: "Delete board",
                            child: GestureDetector(
                              onTap: () => {
                                showDialog<String>(
                                  context: context,
                                  builder: showConfirmDialog(board.id),
                                )
                              },
                              child: (state is DeleteBoardDeleting)
                                  ? CircularProgressIndicator(backgroundColor: Colors.white)
                                  : Icon(
                                      Icons.delete_forever,
                                      size: 24,
                                      color: Theme.of(context).primaryIconTheme.color,
                                    ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      footer: CreateTaskBasic(board.id),
      /*leftSide: VerticalDivider(
        // color: Colors.blue,
        width: 1.5,
        thickness: 1.5,
      ),
      rightSide: VerticalDivider(
        // color: Colors.blue,
        width: 1.5,
        thickness: 1.5,
      ),*/
      children: List.generate(tasks.length, (index) => _buildItem(tasks[index], board.id)),
    );
  }

  _buildItem(TaskDto task, int boardId) {
    return DragAndDropItem(
      child: Tooltip(
        message: "Show detail",
        triggerMode: TooltipTriggerMode.tap,
        waitDuration: Duration(milliseconds: 300),

        verticalOffset: 10,
        child: MouseRegion(
          cursor: SystemMouseCursors.click,
          child: Container(
              margin: EdgeInsets.all(5),
              width: double.infinity,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.white70,
              ),
              child: GestureDetector(
                onTap: () => {Navigator.of(context).pushNamed(UpdateTaskScreen.routeName, arguments: UpdateTaskScreenArgs(taskDto: task, boardId: boardId))},
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(task.name),
                ),
              )),
        ),
      ),
    );
  }

  _onItemReorder(int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    BlocProvider.of<BoardBloc>(context).add(ReorderTasks(oldItemIndex, oldListIndex, newItemIndex, newListIndex));

    setState(() {
      var movedItem = _list[oldListIndex].tasks.removeAt(oldItemIndex);
      _list[newListIndex].tasks.insert(newItemIndex, movedItem);
    });
  }

  _onListReorder(int oldListIndex, int newListIndex) {
    BlocProvider.of<BoardBloc>(context).add(ReorderBoardsEvent(oldListIndex, newListIndex));

    setState(() {
      var movedBoard = _list.removeAt(oldListIndex);
      _list.insert(newListIndex, movedBoard);
    });
  }
}
