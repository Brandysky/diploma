import 'package:app/config/common/app_strings.dart';
import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/presentation/widgets/confirm_dialog.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:app/config/common/globals.dart' as globals;

class Slab extends StatelessWidget {
  final String iconPath;
  final String label;
  final bool confirm;
  final Function function;

  Slab({ @required this.iconPath, @required this.label,  this.confirm = false,  this.function});

  final slabHeight = SizeConfig.screenHeight * .07;
  double iconHeight = (globals.screenWidth == globals.ScreenWidth.SMALL) ? 25 : 35;

  final labelWidth = SizeConfig.screenHeight * .20 * 3 / 4;
  final String dogUrl = 'https://www.svgrepo.com/show/2046/dog.svg';

  showConfirmDialog(Function fce) {
    return (BuildContext context) => ConfirmDialog(fce);
  }

  @override
  Widget build(BuildContext context) {

    return FlatButton(
        onPressed: () {
            if (confirm) {
              showDialog<String>(
                context: context,
                builder: showConfirmDialog(function),
              );
            } else {
              function();
            }

        },
        splashColor:  AppColors.CUSTOM_COLOR_C,
        height: slabHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SvgPicture.asset(
              iconPath,
              color: Theme.of(context).secondaryHeaderColor,
              height: iconHeight,
            ),

            Container(
              child: AutoSizeText(
                label,
                maxLines: 1,
                softWrap: true,
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: AppColors.DARK_BLUE_COLOR,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            )
          ],
        ));
  }
}
