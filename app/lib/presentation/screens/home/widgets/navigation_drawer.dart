import 'package:app/data/store/auth/logout/logout_bloc.dart';
import 'package:app/presentation/screens/dashboard_screen/dashboard_screen.dart';
import 'package:app/presentation/screens/home/home_screen.dart';
import 'package:app/presentation/screens/home/widgets/slab.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationDrawer extends StatelessWidget {
  NavigationDrawer();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.grey.shade200,
      child: ListView(
        children: <Widget>[
          Slab(
            label: "Home",
            iconPath: 'assets/core/icons/logout.svg',
            function: () => {Navigator.of(context).pushNamed(HomeScreen.routeName)},
          ),
          Slab(
            label: "Dashboard",
            iconPath: 'assets/core/icons/logout.svg',
            function: () => {Navigator.of(context).pushNamed(DashboardScreen.routeName)},
          ),
          Slab(
            label: "Sign out",
            confirm: true,
            iconPath: 'assets/core/icons/logout.svg',
            function: () => {
              BlocProvider.of<LogoutBloc>(context).add(LogoutUserEvent(context)),
            },
          ),
        ],
      ),
    );
  }
}
