import 'package:app/config/design/size_config.dart';
import 'package:app/presentation/screens/registerScreen/widgets/register_body.dart';

import 'package:flutter/material.dart';


class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class RegisterScreen extends StatefulWidget {
  static String routeName = "/register";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<RegisterScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();



  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,

      body: RegisterBody(),
    );
  }
}
