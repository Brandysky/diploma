import 'package:app/config/design/app_colors.dart';
import 'package:app/config/design/size_config.dart';
import 'package:app/data/models/user/signup_request.dart';
import 'package:app/data/store/auth/login/auth_bloc.dart';
import 'package:app/presentation/screens/home/home_screen.dart';
import 'package:app/presentation/screens/loginScreen/login_screen.dart';
import 'package:app/presentation/widgets/default_button.dart';
import 'package:app/presentation/widgets/form_info_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/config/common/globals.dart' as globals;

class RegisterBody extends StatefulWidget {
  @override
  _RegisterBodyState createState() => _RegisterBodyState();
}

class _RegisterBodyState extends State<RegisterBody> {
  final loginController = TextEditingController();
  final passwordController = TextEditingController();
  final nameController = TextEditingController();
  bool _inputIsValid = false;
  String _errorMessage = "";
  SignupRequest signupRequest = SignupRequest();

  @override
  void initState() {
    signupRequest = SignupRequest();
    super.initState();
  }

  @override
  void dispose() {
    loginController.dispose();
    passwordController.dispose();
    nameController.dispose();
    super.dispose();
  }

  void setLogin(String login) {
    setState(() {
      signupRequest.login = login;
    });
    inputChanged();
  }

  void setPassword(String password) {
    setState(() {
      signupRequest.password = password;
    });
    inputChanged();
  }

  void setName(String name) {
    setState(() {
      signupRequest.name = name;
    });
    inputChanged();
  }

  bool isPasswordCompliant(String password) {
    if (password == null || password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    // bool hasSpecialCharacters = password.contains(new RegExp(r'[!@#$%^+&*(),.?":{}|<>]'));
    bool hasMinLength = password.length >= 8;

    return hasDigits & hasUppercase & hasMinLength;
  }

  bool validatePass(String pass) {
    String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[$&+,:;=?@#|<>.-^*()%!]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(pass);
  }

  void inputChanged() {
    bool val = true;
    if (signupRequest.name == null || signupRequest.name.isEmpty) val = false;
    if (signupRequest.password == null || signupRequest.password.isEmpty) val = false;
    if (signupRequest.login == null || signupRequest.login.isEmpty) val = false;

    if (signupRequest.login == null || signupRequest.login == "" || signupRequest.login.length < 3) val = false;

    if (signupRequest.password == null || signupRequest.password == "" || !isPasswordCompliant(signupRequest.password)) val = false;

    setState(() {
      _inputIsValid = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: (globals.screenWidth == globals.ScreenWidth.LARGE || globals.screenWidth == globals.ScreenWidth.MEDIUM || globals.screenWidth == globals.ScreenWidth.X_LARGE) ? Center(child: _buildForm()) : _buildForm()),
    );
  }

  Widget _buildForm() {
    return Container(
      width: (globals.screenWidth == globals.ScreenWidth.SMALL || globals.screenWidth == globals.ScreenWidth.MEDIUM)
          ? double.infinity
          : (globals.screenWidth == globals.ScreenWidth.LARGE)
              ? SizeConfig.screenWidth * .5
              : 500,
      padding: EdgeInsets.symmetric(horizontal: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 0 : 20),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20),
            RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                children: [
                  TextSpan(
                    text: 'Registration',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(Icons.account_circle_rounded, size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25, color: Theme.of(context).secondaryHeaderColor),
                title: new TextField(
                  maxLines: 1,
                  controller: nameController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setName(value),
                  decoration: new InputDecoration(labelText: "Name", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(Icons.badge_rounded, size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25, color: Theme.of(context).secondaryHeaderColor),
                title: new TextField(
                  maxLines: 1,
                  controller: loginController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setLogin(value),
                  decoration: new InputDecoration(labelText: "Login", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(Icons.lock_rounded, size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25, color: Theme.of(context).secondaryHeaderColor),
                title: new TextField(
                  maxLines: 1,
                  obscureText: true,
                  controller: passwordController,
                  inputFormatters: [LengthLimitingTextInputFormatter(255)],
                  onChanged: (value) => setPassword(value),
                  decoration: new InputDecoration(labelText: "Password", hintStyle: TextStyle(color: Theme.of(context).colorScheme.primary)),
                ),
              ),
            ),     Container(
              child: new ListTile(
                contentPadding: EdgeInsets.zero,
                minLeadingWidth: 0,
                leading: Icon(
                  Icons.lock_rounded,
                  size: (globals.screenWidth == globals.ScreenWidth.SMALL) ? 15 : 25,
                  color: Colors.transparent,
                ),
                title: Text(
                  "(At least 1 upper-Case, 1 number and 8 characters)",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
              String message = "";
              bool error = false;
              if (state is AuthEvent) {
                message = "Checking";
              } else if (state is AuthLoadedState) {
                message = "Created";
              } else if (state is AuthErrorState) {
                message = state.errorMessage;
                error = true;
              }
              return FormInfoLabel(message, SizeConfig.screenHeight * .05, error);
            }),
            BlocConsumer<AuthBloc, AuthState>(
              listener: (context, state) {
                _errorMessage = "";
                if (state is AuthLoadingState) {
                } else if (state is AuthLoadedState) {
                  print("SAVED");
                  Navigator.of(context).pushNamed(HomeScreen.routeName);
                } else if (state is AuthErrorState) {
                  setState(() {
                    _errorMessage = state.errorMessage;
                  });
                }
              },
              builder: (context, state) {
                return DefaultButton(
                  width: SizeConfig.screenWidth * .85 * .7,
                  text: "Create account",
                  isLoading: state is AuthLoadingState,
                  isEnabled: _inputIsValid,
                  press: () {
                    BlocProvider.of<AuthBloc>(context).add(SignupRequestEvent(signupRequest));
                  },
                );
              },
            ),
            SizedBox(
              height: 30,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(LoginScreen.routeName);
              },
              child: Container(
                margin: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                padding: EdgeInsets.all(15),
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Flexible(
                      child: Text(
                        'Already have an account ?',
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Login',
                      style: TextStyle(
                        color: AppColors.CUSTOM_COLOR_E,
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
