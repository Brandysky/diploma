import 'package:app/config/common/constant.dart';
import 'package:flutter/material.dart';

import 'package:app/presentation/widgets/default_button.dart';
import 'package:app/config/auth/secure_storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TestButtons extends StatelessWidget {


  static String routeName = "/testButtons";

  void fce() async {
    String token = await StorageUtil.read(Constant.TOKEN);
    String deviceToken = await StorageUtil.read(Constant.TOKEN);
    print(token);
    print(deviceToken);
    Map<String, String> a = await StorageUtil.readAll();
    print(a);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DefaultButton(
          width: 200,
          text: "Ukaž tokeny",
          press: () {
            fce();
          },
        ),
        DefaultButton(
          width: 200,
          text: "stary token",
          press: () {
            StorageUtil.write(Constant.TOKEN,"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjY5LCJyb2xlIjoiUk9MRV9VU0VSIiwiaWF0IjoxNjMyMTQzMzYwLCJleHAiOjE2MzIyMjk3NjB9.6ZTgxn8f9zFezPwrfX3gqoO5cFQGyhJuuT7xIyWeSU4Vbptmppm6yx5-OLfJLpkZzuMq1E9YKiUH-R95H-pE-Q");
          },
        ),
        DefaultButton(
          width: 200,
          text: "smazat device token",
          press: () {
            StorageUtil.delete(Constant.DEVICE_TOKEN);
          },
        ),

      /*
        DefaultButton(
          width: 200,
          text: "get sys params",
          press: () {
            BlocProvider.of<AppBloc>(context).add(
             UpdateFromAPIEvent());
          },
        ),
        DefaultButton(
          width: 200,
          text: "get sys params",
          press: () {
            BlocProvider.of<AppBloc>(context).add(
              LoadFromStorageEvent());
          },
        ),*/
      ],
    );
  }
}
