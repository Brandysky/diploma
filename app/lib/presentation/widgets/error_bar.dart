import 'package:flutter/material.dart';

class ErrorBar extends StatelessWidget {
  final String _label;

  ErrorBar(this._label);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(_label,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 14, color: Colors.red)));
  }
}
