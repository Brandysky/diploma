import 'package:app/config/design/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/config/common/globals.dart' as globals;

import 'default_button.dart';

class ConfirmDialog extends StatelessWidget {
  final Function fce;

  ConfirmDialog(this.fce);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: Text(
        'Are you sure?',
        style: Theme.of(context).textTheme.headline4,
        textAlign: TextAlign.center,
      ),
      content: Container(
        child: (globals.screenWidth == globals.ScreenWidth.SMALL)
            ? Column(
                children: [
                  _yesBtn(context),
                  SizedBox(
                    height: 10,
                  ),
                  _noBtn(context)
                ],
              )
            : Row(
          mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _yesBtn(context),
                  SizedBox(
                    width: 10,
                  ),
                  _noBtn(context)
                ],
              ),
      ),
    );
  }

  Widget _yesBtn(BuildContext context) {
    return DefaultButton(
      text: "Yes",
      press: () => {
        Navigator.pop(context, 'OK'),
        fce(),
      },
      width: (globals.screenWidth == globals.ScreenWidth.SMALL) ? SizeConfig.screenWidth :130,
    );
  }

  Widget _noBtn(BuildContext context) {
    return DefaultButton(
      text: "No",
      color: Colors.red,
      press: () => {Navigator.pop(context, 'Cancel')},
      width: (globals.screenWidth == globals.ScreenWidth.SMALL) ? SizeConfig.screenWidth : 130,
    );
  }
}
