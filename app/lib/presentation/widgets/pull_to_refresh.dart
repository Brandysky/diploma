import 'package:app/config/common/app_strings.dart';
import 'package:app/config/design/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PullToRefresh extends StatelessWidget {
  final Function onRefresh;
  final Widget body;
  final RefreshController refreshController;
  PullToRefresh({@required this.onRefresh, @required this.body,  @required this.refreshController});


  Widget headerOnline() {
    return WaterDropHeader(waterDropColor: AppColors.CUSTOM_COLOR_C);
  }

  Widget headerOffline() {
    return CustomHeader(
      builder: (context, mode) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              DEFAULT_OFFLINE_ERROR_MESSAGE,
              style: TextStyle(
                  fontSize: 18,
                  color: AppColors.TEXT_OFF_COLOR
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(
                  Icons.wifi_off_rounded,
                  size: 18,
                  color: AppColors.TEXT_OFF_COLOR
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      header:  headerOffline(),
      controller: refreshController,
      onRefresh: onRefresh,
      child: body,
    );
  }
}
