import 'package:app/config/common/app_strings.dart';
import 'package:app/config/design/app_colors.dart';
import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final String text;
  final Function press;
  final double width;
  final double height;
  final bool isEnabled;
  final bool isLoading;
  final Color color;

  const DefaultButton({Key key, @required this.text, @required this.press, this.width = double.infinity, this.isEnabled = true, this.height = 50, this.isLoading = false, this.color =
  AppColors.CUSTOM_COLOR_E}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
            width: width,
            height: height,
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(

                // backgroundColor: isEnabled? Colors.blue:Colors.grey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                side: BorderSide(width: 2, color: isEnabled? color:Colors.grey),
              ),
              onPressed: isEnabled ? press : () => {},
              child: isLoading
                  ? CircularProgressIndicator(backgroundColor: color)
                  :Text(text,style:TextStyle(color: isEnabled? color:Colors.grey)),
            )
          );
  }
}
