import 'package:flutter/material.dart';

class BodyWithRadiusBorder extends StatelessWidget {
  final Widget beforeBody;
  final Widget body;

  const BodyWithRadiusBorder({
    this.beforeBody,
    @required this.body,
    Key key,
  }) : super(key: key);

  AppBarTheme buildAppBarTheme() {
    return AppBarTheme(
      color: Colors.blue,
      iconTheme: IconThemeData(color: Colors.white),
      elevation: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        beforeBody,
        Expanded(
          child: Container(
            width: double.infinity,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
            ),
            child: body,
          ),
        ),
      ].where((w) => w != null).toList(),
    );
  }
}
