import 'package:flutter/material.dart';

class FormInfoLabel extends StatelessWidget {
  final String errorMessage;
  final double height;
  final double fontHeight = 15;
  final bool error;

  FormInfoLabel(this.errorMessage, this.height, this.error);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height,
      child: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Text(errorMessage,
            softWrap: true,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15,
              color: error?Theme.of(context).errorColor:Theme.of(context).primaryColor,
            )),
      ),
    );
  }
}
