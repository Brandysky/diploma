
import 'package:flutter/material.dart';

class HelpBar extends StatelessWidget {
  final double height;
  final double width;
  final String label;

  HelpBar({@required this.height, @required this.width, @required this.label});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            //                   <--- left side
            color: Theme.of(context).hintColor,
            width: 1,
          ),
        ),

      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // const SizedBox(width: 12.0),
          Icon(
            Icons.info_outline_rounded,
            size: 40,
            color: Theme.of(context).hintColor,
          ),
          const SizedBox(width: 12.0),
          Container(
            width: width * .8,
            child: Text(
              label,
              softWrap: true,
              style: Theme.of(context).textTheme.headline3,
            ),
          )
        ],
      ),
    );
  }
}
