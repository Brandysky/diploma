import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingBar extends StatelessWidget {
  final String _label;

  LoadingBar(this._label);

  @override
  Widget build(BuildContext context) {


    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(height: 10.0),
        const SpinKitWave(color: Colors.blue, type: SpinKitWaveType.start,size:50,itemCount: 10,),
        const SizedBox(height: 10.0),
        Text(_label,style: TextStyle( fontWeight: FontWeight.bold, fontSize: 14))
      ],
    );
  }
}
