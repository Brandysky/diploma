import 'package:app/data/repositories/task_repository.dart';
import 'package:app/data/store/auth/logout/logout_bloc.dart';
import 'package:app/data/store/auth/token/token_bloc.dart';
import 'package:app/data/store/task/create_task/create_task_bloc.dart';
import 'package:app/data/store/task/delete_board/delete_board_bloc.dart';
import 'package:app/presentation/screens/home/home_screen.dart';
import 'package:app/presentation/screens/start_%20screens/offline_screen.dart';
import 'package:app/presentation/screens/start_%20screens/server_down_screen.dart';
import 'package:app/presentation/screens/start_%20screens/splash_screen.dart';
import 'package:app/config/common/globals.dart' as globals;
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:app/data/repositories/authorization_repository.dart';
import 'package:app/data/store/auth/login/auth_bloc.dart';
import 'config/design/theme/theme.dart';
import 'config/routes/routes.dart';

import 'data/repositories/app_repository.dart';
import 'data/store/app/connection/connection_bloc.dart';
import 'data/store/auth/token/token_bloc.dart';
import 'data/store/task/board/board_bloc.dart';
import 'data/store/task/create_board/create_board_bloc.dart';
import 'presentation/screens/loginScreen/login_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BoardBloc bb = BoardBloc(TaskRepository());
  runApp(
    MultiBlocProvider(
      // TODO - DI for repositories
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => AuthBloc(authRepo: AuthorizationRepository()),
        ),
        BlocProvider<TokenBloc>(
          create: (context) => TokenBloc(authRepo: AuthorizationRepository()),
        ),
        BlocProvider<ConnectionBloc>(create: (context) => ConnectionBloc(AppRepository())),
        BlocProvider<LogoutBloc>(create: (context) => LogoutBloc(AuthorizationRepository())),
        BlocProvider<CreateTaskBloc>(create: (context) => CreateTaskBloc(bb, TaskRepository())),
        BlocProvider<BoardBloc>(create: (context) => bb),
        BlocProvider<DeleteBoardBloc>(create: (context) => DeleteBoardBloc(TaskRepository())),
        BlocProvider<CreateBoardBloc>(create: (context) => CreateBoardBloc(TaskRepository())),
      ],
      child: DiplomaApp(),
    ),
  );
}
// flutter pub run build_runner watch --delete-conflicting-outputs

class DiplomaApp extends StatefulWidget {
  @override
  DiplomaAppState createState() => DiplomaAppState();
}

class DiplomaAppState extends State<DiplomaApp> {
  static final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  static final GlobalKey<NavigatorState> _navigatorKey = new GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    // NotificationService().init(_navigatorKey, context);
    BlocProvider.of<ConnectionBloc>(context).add(CheckConnectionEvent());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown, DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    return MaterialApp(
      navigatorKey: _navigatorKey,
      builder: (context, child) {
        return Scaffold(
          key: scaffoldKey,
          body: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              if (constraints.maxWidth <= 320) {
                globals.screenWidth = globals.ScreenWidth.SMALL;
              } else if (constraints.maxWidth <= 600) {
                globals.screenWidth = globals.ScreenWidth.MEDIUM;
              } else if (constraints.maxWidth <= 1200) {
                globals.screenWidth = globals.ScreenWidth.LARGE;
              } else {
                globals.screenWidth = globals.ScreenWidth.X_LARGE;
              }
              return child;
            },
          ),
          resizeToAvoidBottomInset: false,
        );
      },
      home: BlocBuilder<ConnectionBloc, ConnectionBlocState>(builder: (BuildContext context, ConnectionBlocState state) {
        print("type is ${state.runtimeType}");
        switch (state.runtimeType) {
          case ConnectionOnlineLoggedIn:
            return getOnlineScreen();
          case ConnectionOnlineNotLoggedIn:
            return LoginScreen();
          case ConnectionOffline:
            return OfflineScreen();
          case ConnectionOnlineServerDown:
            return ServerDownScreen();
        }
        return SplashScreen();
      }),
      title: 'EasyPlan',
      theme: theme(),
      routes: Routes.getRoutes(),
    );
  }

  Widget getOnlineScreen() {
    //obnova tokenu
    if (BlocProvider.of<TokenBloc>(context).state is TokenStateInitial) BlocProvider.of<TokenBloc>(context).add(CheckTokenRequestEvent());

    return Container(
      child: BlocBuilder<TokenBloc, TokenState>(builder: (BuildContext context, TokenState state) {
        print(state);
        if (state is TokenStateInitial) {
          return SplashScreen();
        } else if (state is TokenStateLoading) {
          return SplashScreen();
        } else if (state is TokenStateLoaded) {
          return HomeScreen();
        } else if (state is TokenStateError) {
          return LoginScreen();
        }
        return SplashScreen();
      }),
    );
  }
}
