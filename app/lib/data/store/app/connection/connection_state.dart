part of 'connection_bloc.dart';

@immutable
abstract class ConnectionBlocState {}

class ConnectionInitial extends ConnectionBlocState {}
class ConnectionChecking extends ConnectionBlocState {}

class ConnectionOnlineLoggedIn extends ConnectionBlocState {}
class ConnectionOnlineNotLoggedIn extends ConnectionBlocState {}
class ConnectionOffline extends ConnectionBlocState {}
class ConnectionOnlineServerDown extends ConnectionBlocState {}
