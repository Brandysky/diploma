part of 'connection_bloc.dart';

@immutable
abstract class ConnectionEvent {}

class CheckConnectionEvent extends ConnectionEvent{}

class ChangeConnectionToOffline extends ConnectionEvent{}
