part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class SignupRequestEvent extends AuthEvent {
  final SignupRequest signupRequestDto;

  SignupRequestEvent(this.signupRequestDto);


}class LoginRequestEvent extends AuthEvent {
  final LoginRequest loginRequest;

  LoginRequestEvent(this.loginRequest);


}



class LogoutEvent extends AuthEvent {}
