import 'dart:async';

import 'package:app/config/common/app_strings.dart';
import 'package:app/config/exception/OcValidationException.dart';
import 'package:app/config/exception/RuntimeException.dart';
import 'package:app/data/models/user/login_request.dart';
import 'package:app/data/models/user/signup_request.dart';
import 'package:app/data/repositories/authorization_repository.dart';
import 'package:app/data/store/app/connection/connection_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthorizationRepository authRepo;

  AuthBloc({this.authRepo}) : super(AuthInitialState());

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is SignupRequestEvent) {
      yield* registerRequest(event.signupRequestDto);
    } else if (event is LoginRequestEvent) {
      yield* loginRequest(event.loginRequest);
    }
  }

  Stream<AuthState> registerRequest(SignupRequest signupRequest) async* {
    yield AuthLoadingState();
    try {
      await authRepo.requestSignUp(signupRequest);
      yield AuthLoadedState();
      await Duration(seconds: 3);
      yield AuthInitialState();

    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield AuthErrorState(errorMessage);
    }
  }

  Stream<AuthState> loginRequest(LoginRequest loginRequest) async* {
    yield AuthLoadingState();
    try {
      await authRepo.requestLogin(loginRequest);
      yield AuthLoadedState();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield AuthErrorState(errorMessage);
    }
  }
}
