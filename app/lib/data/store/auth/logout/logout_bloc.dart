import 'dart:async';

import 'package:app/config/auth/secure_storage.dart';
import 'package:app/config/common/app_strings.dart';
import 'package:app/data/repositories/authorization_repository.dart';
import 'package:app/presentation/screens/loginScreen/login_screen.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

part 'logout_event.dart';
part 'logout_state.dart';

class LogoutBloc extends Bloc<LogoutEvent, LogoutState> {
  final AuthorizationRepository authRepo;

  LogoutBloc(this.authRepo) : super(LogoutInitial());

  @override
  Stream<LogoutState> mapEventToState(
    LogoutEvent event,
  ) async* {
     if( event is LogoutUserEvent){
       yield* logoutUser(event.context);
     }
  }

  Stream<LogoutState> logoutUser(BuildContext context) async*{
    yield LogoutLoading();
    try {
      // await authRepo.logoutUser();

      StorageUtil.deleteAll();

      Navigator.of(context).pushNamedAndRemoveUntil(LoginScreen.routeName,(Route<dynamic> route) => false);
      yield LogoutDone();
      await Future.delayed(Duration(seconds: 3));
      yield LogoutInitial();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      yield LogoutError(errorMessage);
    }
  }
}
