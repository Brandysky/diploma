part of 'logout_bloc.dart';

@immutable
abstract class LogoutState {}

class LogoutInitial extends LogoutState {}
class LogoutLoading extends LogoutState {}
class LogoutDone extends LogoutState {}
class LogoutError extends LogoutState {
  String errorMessage;

  LogoutError(this.errorMessage);
}
