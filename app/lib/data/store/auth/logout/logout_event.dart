part of 'logout_bloc.dart';

@immutable
abstract class LogoutEvent {}

class LogoutUserEvent extends LogoutEvent{
  final BuildContext context;

  LogoutUserEvent(this.context);
}
