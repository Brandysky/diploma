

part of 'token_bloc.dart';

@immutable
abstract class TokenState {}

class TokenStateInitial extends TokenState{}
class TokenStateLoading extends TokenState{}
class TokenStateLoaded extends TokenState{}
class TokenStateError extends TokenState{}
