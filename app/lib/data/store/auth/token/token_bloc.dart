import 'dart:async';
import 'dart:io';

import 'package:app/config/auth/secure_storage.dart';
import 'package:app/config/common/constant.dart';
import 'package:app/data/models/auth/auth_request.dart';
import 'package:app/data/repositories/authorization_repository.dart';
import 'package:app/data/store/auth/token/token_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'token_event.dart';

part 'token_state.dart';

class TokenBloc extends Bloc<TokenEvent, TokenState> {
  final AuthorizationRepository authRepo;

  TokenBloc({this.authRepo}) : super(TokenStateInitial());

  @override
  Stream<TokenState> mapEventToState(TokenEvent event,) async* {
    if (event is CheckTokenRequestEvent) {
      yield TokenStateLoading();
      String token = await StorageUtil.read(Constant.TOKEN);
      String refreshToken = await StorageUtil.read(Constant.DEVICE_TOKEN);
      try {
        if (token == null) throw Exception("Token is null");
        await authRepo.checkToken(token: token);
        if (refreshToken == null)
          throw Exception("RefreshToken is null");
        await authRepo.refreshToken(); //vždy aktualizovat token při zapnutí appky
        yield TokenStateLoaded();
      } catch (e) {
        try{
          await authRepo.refreshToken();
          yield TokenStateLoaded();
        }catch(e){
          yield TokenStateError();
        }
      }
    }
  }
}
