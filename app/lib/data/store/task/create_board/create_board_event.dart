part of 'create_board_bloc.dart';


abstract class CreateBoardEvent {}

class CreateNewBoardEvent extends CreateBoardEvent{

  final BoardDto boardDto;

  CreateNewBoardEvent(this.boardDto);
}

