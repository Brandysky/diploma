part of 'create_board_bloc.dart';

@immutable
abstract class CreateBoardState {}

class CreateBoardInitial extends CreateBoardState {}
class CreateBoardSaving extends CreateBoardState {}
class CreateBoardSaved extends CreateBoardState {}
class CreateBoardError extends CreateBoardState {

  final String errorMessage;

  CreateBoardError(this.errorMessage);
}
