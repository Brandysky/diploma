import 'dart:async';

import 'package:app/config/common/app_strings.dart';
import 'package:app/config/exception/OcValidationException.dart';
import 'package:app/config/exception/RuntimeException.dart';
import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/repositories/task_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';



part 'create_board_event.dart';

part 'create_board_state.dart';

class CreateBoardBloc extends Bloc<CreateBoardEvent, CreateBoardState> {
  final TaskRepository taskRepository;

  CreateBoardBloc(this.taskRepository) : super(CreateBoardInitial());

  @override
  Stream<CreateBoardState> mapEventToState(
    CreateBoardEvent event,
  ) async* {
    if (event is CreateNewBoardEvent) {
      yield* createBoardEvent(event.boardDto);
    }
    BoardDto dto;
  }

  Stream<CreateBoardState> createBoardEvent(BoardDto boardDto) async* {
    yield CreateBoardSaving();
    try {
      await taskRepository.saveNewBoard(boardDto);
      yield CreateBoardSaved();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield CreateBoardError(errorMessage);
    }
  }
}
