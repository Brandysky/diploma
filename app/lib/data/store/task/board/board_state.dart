part of 'board_bloc.dart';

@immutable
abstract class BoardState {}

class BoardInitial extends BoardState {}
class BoardLoading extends BoardState {}
class BoardLoaded extends BoardState {
  final List<BoardDto> boards;
  BoardLoaded(this.boards);
}

class BoardErrorState extends BoardState {
  final String errorMessage;
  BoardErrorState(this.errorMessage);
}
