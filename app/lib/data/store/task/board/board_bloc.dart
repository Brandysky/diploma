import 'dart:async';

import 'package:app/config/common/app_strings.dart';
import 'package:app/config/exception/OcValidationException.dart';
import 'package:app/config/exception/RuntimeException.dart';
import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/repositories/task_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'board_event.dart';

part 'board_state.dart';

class BoardBloc extends Bloc<BoardEvent, BoardState> {
  final TaskRepository taskRepository;

  BoardBloc(this.taskRepository) : super(BoardInitial());

  @override
  Stream<BoardState> mapEventToState(
    BoardEvent event,
  ) async* {
    if (event is BoardLoadEvent) {
      yield* loadBoards();
    }
  }

  Stream<BoardState> loadBoards() async* {
    yield BoardLoading();
    try {
      List<BoardDto> boards = await taskRepository.loadBoards();
      yield BoardLoaded(boards);
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield BoardErrorState(errorMessage);
    }
  }
}
