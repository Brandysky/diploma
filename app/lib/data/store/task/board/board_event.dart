part of 'board_bloc.dart';

@immutable
abstract class BoardEvent {}

class BoardLoadEvent extends BoardEvent{

}

class ReorderBoardsEvent extends BoardEvent{

  final int oldListIndex;
  final int newListIndex;

  ReorderBoardsEvent(this.oldListIndex, this.newListIndex);
}

class ReorderTasks extends BoardEvent {
  final int oldItemIndex;
  final int oldListIndex;
  final int newItemIndex;
  final int newListIndex;

  ReorderTasks(this.oldItemIndex, this.oldListIndex, this.newItemIndex, this.newListIndex);
}
