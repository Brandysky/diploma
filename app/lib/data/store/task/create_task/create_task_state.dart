part of 'create_task_bloc.dart';

@immutable
abstract class CreateTaskState {}

class CreateTaskInitial extends CreateTaskState {}
class CreateTaskSaving extends CreateTaskState {}
class CreateTaskSaved extends CreateTaskState {}
class CreateTaskError extends CreateTaskState {

  final String errorMessage;

  CreateTaskError(this.errorMessage);
}
