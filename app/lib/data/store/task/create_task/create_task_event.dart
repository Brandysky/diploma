part of 'create_task_bloc.dart';

abstract class CreateTaskEvent {}

class CreateNewTaskEvent extends CreateTaskEvent {
  final TaskDto taskDto;
  final int boardId;

  CreateNewTaskEvent(this.taskDto, this.boardId);
}

class UpdateTaskEvent extends CreateTaskEvent {
  final TaskDto taskDto;
  final int boardId;

  UpdateTaskEvent(this.taskDto, this.boardId);
}

class DeleteTaskEvent extends CreateTaskEvent{
  final int taskId;

  DeleteTaskEvent(this.taskId);
}


