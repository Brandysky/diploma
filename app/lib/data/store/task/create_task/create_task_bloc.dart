import 'dart:async';

import 'package:app/config/common/app_strings.dart';
import 'package:app/config/exception/OcValidationException.dart';
import 'package:app/config/exception/RuntimeException.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/data/repositories/task_repository.dart';
import 'package:app/data/store/task/board/board_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'create_task_event.dart';

part 'create_task_state.dart';

class CreateTaskBloc extends Bloc<CreateTaskEvent, CreateTaskState> {
  final TaskRepository taskRepository;
  final BoardBloc boardBloc;

  CreateTaskBloc(this.boardBloc,this.taskRepository) : super(CreateTaskInitial());

  @override
  Stream<CreateTaskState> mapEventToState(
    CreateTaskEvent event,
  ) async* {
    if (event is CreateNewTaskEvent) {
      yield* createNewTask(event.taskDto, event.boardId);
    } else if (event is DeleteTaskEvent) {
      yield* deleteTask(event.taskId);
    }else if (event is UpdateTaskEvent) {
      yield* update(event.taskDto,event.boardId);
    }
  }

  Stream<CreateTaskState> deleteTask(int taskId) async* {
    await taskRepository.deleteTask(taskId);
    boardBloc.add(BoardLoadEvent());

  }

  Stream<CreateTaskState> createNewTask(TaskDto taskDto, int boardId) async* {
    yield CreateTaskSaving();
    try {
      await taskRepository.saveNewTask(taskDto, boardId);
      yield CreateTaskSaved();

      await Duration(seconds: 3);
      yield CreateTaskInitial();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield CreateTaskError(errorMessage);

      await Duration(seconds: 3);
      yield CreateTaskInitial();
    }
  }
  Stream<CreateTaskState> update(TaskDto taskDto, int boardId) async* {
    yield CreateTaskSaving();
    try {
      await taskRepository.update(taskDto, boardId);
      yield CreateTaskSaved();

      await Duration(seconds: 3);
      yield CreateTaskInitial();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield CreateTaskError(errorMessage);

      await Duration(seconds: 3);
      yield CreateTaskInitial();
    }
  }
}
