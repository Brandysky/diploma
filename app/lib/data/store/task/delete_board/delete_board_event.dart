part of 'delete_board_bloc.dart';


abstract class DeleteBoardEvent {}

class DeleteNewBoardEvent extends DeleteBoardEvent{

  final int boardId;

  DeleteNewBoardEvent(this.boardId);
}

