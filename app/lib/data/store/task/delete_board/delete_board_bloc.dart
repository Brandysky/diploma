import 'dart:async';

import 'package:app/config/common/app_strings.dart';
import 'package:app/config/exception/OcValidationException.dart';
import 'package:app/config/exception/RuntimeException.dart';
import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/repositories/task_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';


part 'delete_board_event.dart';

part 'delete_board_state.dart';

class DeleteBoardBloc extends Bloc<DeleteBoardEvent, DeleteBoardState> {
  final TaskRepository taskRepository;

  DeleteBoardBloc(this.taskRepository) : super(DeleteBoardInitial());

  @override
  Stream<DeleteBoardState> mapEventToState(
      DeleteBoardEvent event,
  ) async* {
    if (event is DeleteNewBoardEvent) {
      yield* deleteBoardEvent(event.boardId);
    }
    BoardDto dto;
  }

  Stream<DeleteBoardState> deleteBoardEvent(int boardId) async* {
    yield DeleteBoardDeleting();
    try {
      await taskRepository.deleteBoard(boardId);
      yield DeleteBoardDeleted();
    } catch (e) {
      String errorMessage = DEFAULT_ERROR_MESSAGE;
      if (e is OcValidationException) {
        errorMessage = e.getFirstMessageOrDefault;
      } else if (e is RuntimeException) {
        errorMessage = e.message;
      }
      yield DeleteBoardError(errorMessage);
    }
  }
}
