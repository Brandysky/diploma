part of 'delete_board_bloc.dart';

@immutable
abstract class DeleteBoardState {}

class DeleteBoardInitial extends DeleteBoardState {}
class DeleteBoardDeleting extends DeleteBoardState {}
class DeleteBoardDeleted extends DeleteBoardState {}
class DeleteBoardError extends DeleteBoardState {

  final String errorMessage;

  DeleteBoardError(this.errorMessage);
}
