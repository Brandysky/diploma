import 'package:app/config/common/utils.dart';
import 'package:flutter/cupertino.dart';

class MessageDto {
  int id;
  String title;
  String body;
  DateTime activeFrom;
  DateTime activeTo;
  String segment;
  int segmentId;
  bool dateFromPicked;
  bool dateToPicked;
  DateTime created;
  bool seen;

  MessageDto(
      {@required this.title,
      @required this.body,
      @required this.activeFrom,
      @required this.activeTo,
      @required this.segment,
      @required this.dateFromPicked,
      @required this.dateToPicked,
      @required this.segmentId,
      this.created,
      this.id,
      this.seen});

  static MessageDto fromJson(dynamic json) {
    if(json == null) return null;
    return MessageDto(
      id: json['id'],
      title: json['title'],
      body: json['body'],
      activeFrom: Utils.dateTimeFromJson(json['activeFrom']),
      activeTo: Utils.dateTimeFromJson(json['activeTo']),
      segment: json['segment'],
      segmentId: json['segmentId'],
      created:Utils.dateTimeFromJson(json['created']),
      seen: json['seen'],
    );
  }

  Map<String, dynamic> toJson() => {
        "title": title,
        "body": body,
        "activeFrom": dateFromPicked?Utils.dateTimeToJsonString(activeFrom):null,
        "activeTo": dateToPicked?Utils.dateTimeToJsonString(activeTo):null,
        "segment": segment,
        "segmentId": segmentId,
        "created": dateToPicked?Utils.dateTimeToJsonString(created):null,
        "seen": seen
      };
}

class NotificationDto {
  String title;
  String body;
  String segment;
  int segmentId;

  NotificationDto({this.title, this.body, this.segment, this.segmentId});

  Map<String, dynamic> toJson() => {
        "title": title,
        "body": body,
        "segment": segment,
        "segmentId": segmentId
      };
}

class SegmentGroupDto {
  String groupName;
  List<MessageSegmentDto> segments = [];

  SegmentGroupDto({this.groupName, this.segments});

  static SegmentGroupDto fromJson(dynamic json) {
    if(json == null) return null;
    return SegmentGroupDto(
      groupName: json['groupName'],
      segments: ((json['segments']) as List)
          .map((i) => MessageSegmentDto.fromJson(i))
          .toList(),
    );
  }


}

class MessageSegmentDto {
  int id;
  String name;

  MessageSegmentDto({this.id, this.name});

  static MessageSegmentDto fromJson(dynamic json) {
    if(json == null) return null;
    return MessageSegmentDto(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name
  };
}
