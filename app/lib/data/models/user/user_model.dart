import 'package:app/config/common/utils.dart';
import 'package:app/data/models/admin/admin_models.dart';
import 'package:app/data/models/user/pay_status_enum.dart';

class IssueComment {
  int id;
  String body;
  DateTime dateCreated;
  int issueId;
  UserDetail user;

  IssueComment({this.id, this.body, this.dateCreated, this.user,this.issueId});

  static IssueComment fromJson(dynamic json) {


    return IssueComment(
        id: json['id'],
        body: json['body'],
        dateCreated: Utils.dateTimeFromJson(json['dateCreated']),
        user: UserDetail.fromJson(json['user']));
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "body": body,
    "dateCreated":  Utils.dateTimeToJsonString(dateCreated),
    "issueId":  issueId,
    "user": user==null?null:user.toJson(),
  };
}

class Issue {
  int id;
  String title;
  String body;
  DateTime dateCreated;
  DateTime dueDate;
  bool done;
  bool dueDatePicked;
  bool mine;
  List<IssueComment> comments;
  UserDetail userCreated;
  UserDetail userWithIssue;
  int userWithIssueFormValue;
  UserDetail userAssigned;
  int userAssignedFormValue;

  Issue(
      {this.id,
      this.title,
      this.body,
      this.dateCreated,
      this.dueDate,
      this.done,
      this.mine,
      this.comments,
      this.userCreated,
      this.userWithIssue,
      this.userWithIssueFormValue,
      this.userAssignedFormValue,
      this.userAssigned});

  static Issue fromJson(dynamic json) {
    return Issue(
        id: json['id'],
        title: json['title'],
        body: json['body'],
        dateCreated: Utils.dateTimeFromJson(json['dateCreated']),
        dueDate: Utils.dateTimeFromJson(json['dueDate']),
        done: json['done'],
        mine: json['mine'],
        comments: issueCommentFromJson(json['comments']),
        userWithIssueFormValue: json['userWithIssueFormValue'],
        userAssignedFormValue: json['userAssignedFormValue'],
        userCreated: UserDetail.fromJson(json['userCreated']),
        userWithIssue: UserDetail.fromJson(json['userWithIssue']),
        userAssigned: UserDetail.fromJson(json['userAssigned']));
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "body": body,
    "dateCreated":  Utils.dateTimeToJsonString(dateCreated),
    "dueDate":  Utils.dateTimeToJsonString(dueDate),
    "done": done,
    "mine": mine,
    "userWithIssueFormValue": userWithIssueFormValue,
    "userAssignedFormValue": userAssignedFormValue,
    "userCreated": userCreated!= null?userCreated.toJson():null,
    "userWithIssue": userWithIssue!= null?userWithIssue.toJson():null,
    "userAssigned": userAssigned!= null?userAssigned.toJson():null
  };

  static List<IssueComment>  issueCommentFromJson(List<dynamic> body) {
    if (body == null) return [];
    return (body.map((i) => IssueComment.fromJson(i)).toList());
  }
}

class AppProblemReport {
  String message;

  AppProblemReport({this.message});

  Map<String, dynamic> toJson() => {"message": message};
}

class InvoicesModel {
  final double sumCelkem;
  final double zbyvaUhradit;
  final DateTime datVyst;
  final DateTime datSplat;
  final DateTime datUhr;
  final String kod;
  final String bic;
  final String iban;
  final String stavUhrK;
  final PayStatusEnum payStatusApp;
  final int id;
  final int companyId;
  final String mena;
  final String popis;
  final String varSym;
  final String konSym;

  InvoicesModel(
      {this.sumCelkem,
      this.zbyvaUhradit,
      this.datVyst,
      this.datSplat,
      this.datUhr,
      this.kod,
      this.iban,
      this.bic,
      this.stavUhrK,
      this.payStatusApp,
      this.id,
      this.companyId,
      this.mena,
      this.popis,
      this.konSym,
      this.varSym});

  static InvoicesModel fromJson(dynamic json) {
    return InvoicesModel(
      sumCelkem: json['sumCelkem'],
      zbyvaUhradit: json['zbyvaUhradit'],
      datVyst: Utils.dateTimeFromJson(json['datVyst']),
      //DateTime.parse(json['datVyst']) ?? null,
      datSplat: Utils.dateTimeFromJson(json['datSplat']),
      //DateTime.parse(json['datSplat']) ?? null,
      datUhr: Utils.dateTimeFromJson(json['datUhr']),
      //DateTime.parse(json['datUhr']) ?? null,
      kod: json['kod'],
      stavUhrK: json['stavUhrK'],
      id: json['id'],
      companyId: json['companyId'],
      mena: json['mena'],
      iban: json['iban'],
      bic: json['bic'],
      popis: json['popis'],
      konSym: json['konSym'],
      varSym: json['varSym'],
      payStatusApp: getPayStatusEnumFromString(json['payStatusApp']),
    );
  }
}

class UserDetail {
  int id;
  String firstName;
  String lastName;
  String address;
  String email;

  //custom
  String fullName;

  UserDetail(
      {this.id,
      this.firstName,
      this.lastName,
      this.address,
      this.email,}) {
    //custom fields
    this.fullName = (this.firstName == null ? "" : this.firstName + " ") +
        (this.lastName == null ? "" : this.lastName);
   }

  static UserDetail emptyUser() {
    return UserDetail(
        firstName: "",
        lastName: "",
        address: "",
        email: "",);
  }

  static UserDetail fromJson(dynamic json) {
    if(json == null) return null;
    return UserDetail(
        id: json['id'],
        firstName: json['firstName'],
        lastName: json['lastName'],
        address: json['address'],
        email: json['email']);
  }



  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
      };
}

class GroupDto {
  String name;
  int id;
  bool active;

  GroupDto({this.name, this.id, this.active});

  static GroupDto fromJson(dynamic json) {
    return GroupDto(id: json['id'], name: json['name'], active: json['active']);
  }

  Map<String, dynamic> toJson() => {"id": id, "name": name, "active": active};
}

class NetworkTestDetailDto {
  double internetUpload;
  double internetDownload;
  double antennaUpload;
  double antennaDownload;
  bool attention;
  bool antennaAllowed;
  DateTime date;
  String publicIp;

  NetworkTestDetailDto(
      {this.internetUpload,
      this.internetDownload,
      this.antennaUpload,
      this.antennaDownload,
      this.attention,
      this.antennaAllowed,
      this.date,
      this.publicIp});

  static NetworkTestDetailDto fromJson(dynamic json) {
    return NetworkTestDetailDto(
      internetUpload: json['internetUpload'],
      internetDownload: json['internetDownload'],
      antennaUpload: json['antennaUpload'],
      antennaDownload: json['antennaDownload'],
      attention: json['attention'],
      antennaAllowed: json['antennaAllowed'],
      publicIp: json['publicIp'],
      date: Utils.dateTimeFromJson(json['date']),
    );
  }

  Map<String, dynamic> toJson() => {
        "internetUpload": internetUpload,
        "internetDownload": internetDownload,
        "antennaUpload": antennaUpload,
        "antennaDownload": antennaDownload,
        "attention": attention,
        "publicIp": publicIp,
        "antennaAllowed": antennaAllowed,
        "date": Utils.dateTimeToJsonString(date)
      };
}

class AccessPointDto {
  int id;
  String name;
  String ssid;
  String tariff;
  String address;
  String contract;
  String state;
  DateTime stateFrom;


  AccessPointDto({this.id, this.name, this.ssid, this.tariff, this.address,
      this.contract, this.state, this.stateFrom});

  static AccessPointDto fromJson(dynamic json) {
    return AccessPointDto(
        id: json['id'],
        name: json['name'],
        ssid: json['ssid'],
        stateFrom: Utils.dateTimeFromJson(json['stateFrom']),
        address: json['address'],
        contract: json['contract'],
        state: json['state'],
        tariff: json['tariff']);
  }

  Map<String, dynamic> toJson() =>
      {"id": id, "name": name, "ssid": ssid, "tariff": tariff};
}
