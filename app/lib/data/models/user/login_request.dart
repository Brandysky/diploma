class LoginRequest {
  String login;
  String password;
  String name;

  LoginRequest({this.login, this.password, this.name});

  static LoginRequest fromJson(dynamic json) {
    return LoginRequest(login: json['login'], password: json['password']);
  }

  Map<String, dynamic> toJson() => {
        "login": login,
        "password": password
      };
}
