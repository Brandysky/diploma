class SignupRequest {
  String login;
  String password;
  String name;

  SignupRequest({this.login, this.password, this.name});

  static SignupRequest fromJson(dynamic json) {
    return SignupRequest(login: json['login'], password: json['password'], name: json['name']);
  }

  Map<String, dynamic> toJson() => {
        "login": login,
        "password": password,
        "name": name,
      };
}
