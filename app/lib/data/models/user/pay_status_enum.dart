

enum PayStatusEnum {
  PAID, //green
  AT_MATURITY, //yellow
  UNPAID, //red
  PARTIALLY_PAID, //yellow + částka
  PARTIALLY_PAID_AFTER_MATURITY //red + částka

}
  PayStatusEnum getPayStatusEnumFromString(String payStatusString) {
    for (PayStatusEnum element in PayStatusEnum.values) {
      if ("PayStatusEnum.$payStatusString" == element.toString()) {
        return element;
      }
    }
    return null;
  }
