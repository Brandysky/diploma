import 'TaskDto.dart';

class BoardDto {
  int id;
  String name;
  int order;
  List<TaskDto> tasks = [];

  BoardDto({this.id, this.name, this.tasks, this.order});

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "order": order,
        "tasks": tasks,
      };

  static BoardDto fromJson(dynamic json) {
    return BoardDto(id: json['id'], name: json['name'], order: json['order'], tasks: tasksFromJson(json['tasks']));
  }

  static List<TaskDto> tasksFromJson(List<dynamic> body) {
    if (body == null) return [];
    return (body.map((i) => TaskDto.fromJson(i)).toList());
  }
}
