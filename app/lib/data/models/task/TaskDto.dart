import 'package:app/config/common/utils.dart';
import 'package:flutter/cupertino.dart';

class TaskDto {
  int id;
  String name;
  String body;
  bool done;
  DateTime dateCreated;
  DateTime dueDate;
  DateTime doneDate;
  String latitude;
  String longitude;
  bool useGPS;
  bool remind;
  REPEAT_INTERVAL repeatInterval;
  List<TaskItemDto> items = [];
  TaskStyleDto taskStyle;

  TaskDto({this.id, this.name, this.body, this.done, this.dateCreated, this.dueDate, this.doneDate, this.latitude, this.longitude, this.useGPS, this.remind, this.repeatInterval, this.items, this.taskStyle}); //todo

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "body": body,
        "done": done,
        "dateCreated": Utils.dateTimeToJsonString(dateCreated),
        "dueDate": Utils.dateTimeToJsonString(dueDate),
        "doneDate": Utils.dateTimeToJsonString(doneDate),
        "latitude": latitude,
        "longitude": longitude,
        "useGPS": useGPS,
        "remind": remind,
        "repeatInterval": repeatInterval,
        "taskStyle": null, //fixme
        "items": items,
      };

  // static TaskDto init(){
  //   return TaskDto(String);
  // }

  static TaskDto fromJson(dynamic json) {
    return TaskDto(
        id: json['id'],
        name: json['name'],
        body: json['body'],
        done: json['done'],
        dateCreated: Utils.dateTimeFromJson(json['dateCreated']),
        dueDate: Utils.dateTimeFromJson(json['dueDate']),
        doneDate: Utils.dateTimeFromJson(json['doneDate']),
        latitude: json['latitude'],
        longitude: json['longitude'],
        useGPS: json['useGPS'],
        remind: json['remind'],
        repeatInterval: getIntervalEnumFromString(json['repeatInterval']),
        taskStyle: json['taskStyle'],
        items: itemsFromJson(json['items']));
  }

  static List<TaskItemDto>  itemsFromJson(List<dynamic> body) {
    if (body == null) return [];
    return (body.map((i) => TaskItemDto.fromJson(i)).toList());
  }

}

enum REPEAT_INTERVAL { DAILY, WEEKLY, MONTHLY }

REPEAT_INTERVAL getIntervalEnumFromString(String interval) {
  for (REPEAT_INTERVAL element in REPEAT_INTERVAL.values) {
    if ("REPEAT_INTERVAL.$interval" == element.toString()) {
      return element;
    }
  }
  return null;
}

class TaskStyleDto {}

class TaskItemDto {
  int id;
  String body;
  bool done;
  DateTime dateCreated;
  DateTime doneDate;
  FocusNode focusNode;
  UniqueKey key;

  Map<String, dynamic> toJson() => {
        "id": id,
        "body": body,
        "done": done,
        "dateCreated": Utils.dateTimeToJsonString(dateCreated),
        "doneDate": Utils.dateTimeToJsonString(doneDate),
      };


  TaskItemDto({this.id, this.body, this.done, this.dateCreated, this.doneDate,this.focusNode,this.key});

  static TaskItemDto fromJson(dynamic json) {
    return TaskItemDto(
        id: json['id'],
        body: json['body'],
        done: json['done'],
        dateCreated: Utils.dateTimeFromJson(json['dateCreated']),
        doneDate: Utils.dateTimeFromJson(json['doneDate']));
  }
}
