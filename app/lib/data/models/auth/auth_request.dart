import 'dart:convert';

import 'package:flutter/material.dart';

String authRequestToJson(AuthRequest data) => json.encode(data.toJson());

class AuthRequest {
  AuthRequest({
    @required this.mobileNumber,
  });

  AuthRequest.otp({
    @required this.mobileNumber,
    @required this.otpCode,
  });

  String mobileNumber;
  String otpCode;

  Map<String, dynamic> toJson() => {
        "mobileNumber": mobileNumber,
        "otpCode": otpCode,
      };
}

class AuthResponse {
  String jwtToken;
  String deviceToken;

  AuthResponse({this.jwtToken, this.deviceToken});

  AuthResponse.fromJson(Map<String, dynamic> json) {
    jwtToken = json['jwtToken'];
    deviceToken = json['deviceToken'];
  }

  Map<String, dynamic> toJson() =>
      {"jwtToken": jwtToken, "deviceToken": this.deviceToken};
}

class CheckToken {
  String token;

  CheckToken({this.token});

  CheckToken.fromJson(Map<String, dynamic> json) {
    token = json['token'];
  }

  Map<String, dynamic> toJson() => {
        "token": token,
      };
}
