import 'dart:convert';
import 'dart:io';

import 'package:app/config/auth/secure_storage.dart';
import 'package:app/config/common/constant.dart';
import 'package:app/data/models/auth/auth_request.dart';
import 'package:app/data/models/user/login_request.dart';
import 'package:app/data/models/user/signup_request.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/config/common/http_client.dart';
import 'package:http/http.dart';
import 'package:jwt_decoder/jwt_decoder.dart';


class AuthorizationRepository {
  static const String _POST_REGISTER_PATH = "/api/login/signup";
  static const String _POST_LOGOUT = "/api/login/logout";
  static const String _POST_LOGIN_PATH = "/api/login/signin";
  static const String _POST_CHECK_TOKEN_PATH = "/api/login/checkToken";
  static const String _POST_CHECK_REFRESH_TOKEN_PATH =
      "/api/login/refreshDeviceToken";


  Future<void> requestSignUp(SignupRequest signupRequest) async {
    Response r = await HttpClient.post(
      path: _POST_REGISTER_PATH,
      body: signupRequest.toJson(),
    );

    AuthResponse ar =
    AuthResponse.fromJson(jsonDecode(utf8.decode(r.bodyBytes)));
    await StorageUtil.write(Constant.DEVICE_TOKEN , ar.deviceToken);
    await StorageUtil.write(Constant.TOKEN, ar.jwtToken);

  }

  Future<void> requestLogin(LoginRequest loginRequest) async {
    Response r = await HttpClient.post(
      path: _POST_LOGIN_PATH,
      body: loginRequest.toJson(),
    );

    AuthResponse ar =
    AuthResponse.fromJson(jsonDecode(utf8.decode(r.bodyBytes)));
    await StorageUtil.write(Constant.DEVICE_TOKEN , ar.deviceToken);
    await  StorageUtil.write(Constant.TOKEN, ar.jwtToken);

  }



  Future<void> checkToken({@required String token}) async {
    Response response = await HttpClient.post(
      path: _POST_CHECK_TOKEN_PATH,
      body: CheckToken(token: token).toJson(),
    );
  }


  Future<void> refreshToken() async {
    String refreshToken = await StorageUtil.read(Constant.DEVICE_TOKEN);
    String urlWithParams = _POST_CHECK_REFRESH_TOKEN_PATH + "/$refreshToken";
    Response response = await HttpClient.post(
      path: urlWithParams,
    );
    AuthResponse authResponse =
        AuthResponse.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));

    await  StorageUtil.write(Constant.DEVICE_TOKEN, authResponse.deviceToken);
    await  StorageUtil.write(Constant.TOKEN, authResponse.jwtToken);
  }

  static Future<bool> isAdminByJWT() async {
    String token = await StorageUtil.read(Constant.TOKEN);
    if (token == null) return false;

    /* decode() method will decode your token's payload */
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token);

    String role = decodedToken["role"];
    if (role == null) return false;
    if (role == "ROLE_ADMIN") return true;
    return false;
  }



  static Future<bool> isTokenExpired() async {
    String token = await StorageUtil.read(Constant.TOKEN);
    if (token == null) return true;

    /* decode() method will decode your token's payload */
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
    print("_"+decodedToken["exp"]);
    var timestamp = decodedToken["exp"];
    if (timestamp == null) return true;
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    if (date.isBefore(DateTime.now())) return true;
    return false;
  }


  Future<void> logoutUser() async {
    await HttpClient.post(path: _POST_LOGOUT);
  }
}
