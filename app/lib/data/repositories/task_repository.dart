import 'dart:convert';

import 'package:app/data/models/task/BoardDto.dart';
import 'package:app/data/models/task/TaskDto.dart';
import 'package:app/config/common/http_client.dart';
import 'package:http/http.dart';

class TaskRepository {
  static const String _POST_UPDATE_TASK = "/api/task/update";
  static const String _POST_CREATE_TASK = "/api/task/new";
  static const String _GET_ALL_BOARDS = "/api/board/all";
  static const String _POST_NEW_BOARD = "/api/board/new";
  static const String _POST_DELETE_BOARD = "/api/board/delete";
  static const String _POST_DELETE_TASK = "/api/task/delete";
  static const String _POST_UPDATE_BOARD_ORDER = "/api/board/updateOrder";
  static const String _POST_UPDATE_TASK_ORDER = "/api/task/updateOrder";

  Future<void> saveNewTask(TaskDto taskDto,int boardId) async {
    var queryParameters = {"boardId": boardId.toString()};
    await HttpClient.post(path: _POST_CREATE_TASK, body: taskDto.toJson(),queryParameters: queryParameters);
  }

  Future<List<BoardDto>> loadBoards() async {
    Response r = await HttpClient.get(_GET_ALL_BOARDS);
    List<dynamic> list = (jsonDecode(utf8.decode(r.bodyBytes)) as List);

    return list.map((i) => BoardDto.fromJson(i)).toList();
  }

  saveNewBoard(BoardDto boardDto) async{
    await HttpClient.post(path: _POST_NEW_BOARD, body: boardDto.toJson());

  }

  Future<void> deleteBoard(int boardId) async {
    var queryParameters = {"boardId": boardId.toString()};
    await HttpClient.post(path: _POST_DELETE_BOARD, queryParameters: queryParameters);
  }
  Future<void> deleteTask(int taskId) async {
    var queryParameters = {"taskId": taskId.toString()};
    await HttpClient.post(path: _POST_DELETE_TASK, queryParameters: queryParameters);
  }

  Future<void> reorderBoards(int oldIndex, int newIndex) async {
    var queryParameters = {
      "oldIndex": oldIndex.toString(),
      "newIndex": newIndex.toString(),
    };
    await HttpClient.post(path: _POST_UPDATE_BOARD_ORDER, queryParameters: queryParameters);
  }
  Future<void> reorderTasks(int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) async {
    var queryParameters = {
      "oldItemIndex": oldItemIndex.toString(),
      "oldListIndex": oldListIndex.toString(),
      "newItemIndex": newItemIndex.toString(),
      "newListIndex": newListIndex.toString(),
    };
    await HttpClient.post(path: _POST_UPDATE_TASK_ORDER, queryParameters: queryParameters);
  }

  update(TaskDto taskDto, int boardId) async{
    var queryParameters = {"boardId": boardId.toString()};
    await HttpClient.post(path: _POST_UPDATE_TASK, body: taskDto.toJson(),queryParameters: queryParameters);
  }
}
